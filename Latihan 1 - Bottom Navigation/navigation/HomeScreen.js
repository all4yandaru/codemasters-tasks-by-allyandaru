import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';

const Tab = createBottomTabNavigator();

function HomeScreen(navigation) {
  return (
    <SafeAreaView style={styles.safeParentContainer}>
      <ScrollView style={styles.mainContent}>
        <Image
          style={{width: '100%'}}
          source={require('../images/marlinBanner.png')}
        />
        <View style={styles.menuContent}>
          <Image source={require('../images/ic_ferry_intl.png')} />
          <Image source={require('../images/ic_ferry_domestic.png')} />
          <Image source={require('../images/ic_attraction.png')} />
          <Image source={require('../images/ic_pioneership.png')} />
        </View>
      </ScrollView>
      <View style={styles.footerContent}>
        <TouchableOpacity style={styles.buttonMore}>
          <Text style={styles.buttonMoreText}>More ...</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  safeParentContainer: {
    flex: 1,
  },
  mainContent: {
    backgroundColor: '#F4F4F4',
    flexGrow: 1,
  },
  menuContent: {
    marginTop: 22,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  buttonMore: {
    backgroundColor: '#2E3283',
    height: 40,
    borderRadius: 10,
    justifyContent: 'center',
    paddingVertical: 9,
    paddingHorizontal: 27,
    marginVertical: 13,
  },
  buttonMoreText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
  },
  footerContent: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    alignItems: 'center',
  },
});

export default HomeScreen;
