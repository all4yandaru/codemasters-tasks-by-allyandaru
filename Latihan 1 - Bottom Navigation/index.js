/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import NavigationContent from './NavigationContent';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => NavigationContent);
