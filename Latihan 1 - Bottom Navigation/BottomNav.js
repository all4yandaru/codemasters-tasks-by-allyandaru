import React, {Component} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';

// navigation
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import IonIcons from 'react-native-vector-icons/Ionicons';

// screen name
const homeName = 'Home';
const detailName = 'Detail';
const profileName = 'Profile';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function HomeScreen(navigation) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text onPress={() => navigation.navigate('Home')}>Home Screen</Text>
    </View>
  );
}

function DetailScreen(navigation) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text onPress={() => navigation.navigate('Detail')}>Detail Screen</Text>
    </View>
  );
}
function ProfileScreen(navigation) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text onPress={() => navigation.navigate('Profile')}>Profile Screen</Text>
    </View>
  );
}

const HomeScreenContainer = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            let rn;

            if (rn == homeName) {
              iconName = focused ? 'home' : 'home-outline';
            } else if (rn == detailName) {
              iconName = focused ? 'detail' : 'detail-outline';
            } else if (rn == profileName) {
              iconName = focused ? 'profile' : 'profile-outline';
            }

            return <IonIcons name={iconName} size={size} color={color} />;
          },
        })}
        initialRouteName={homeName}>
        <Tab.Screen name={homeName} component={HomeScreen} />
        <Tab.Screen name={detailName} component={DetailScreen} />
        <Tab.Screen name={profileName} component={ProfileScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default HomeScreenContainer;
