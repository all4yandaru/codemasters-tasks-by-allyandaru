import { useToast } from '@chakra-ui/react';

export default () => {
  const toastMe = useToast();

  const ShowToast = (title, message, status) => {
    return toastMe({
      position: 'top-right',
      title: title,
      description: message,
      status: status,
      duration: 2000,
      isClosable: true,
    });
  };

  return { ShowToast };
};
