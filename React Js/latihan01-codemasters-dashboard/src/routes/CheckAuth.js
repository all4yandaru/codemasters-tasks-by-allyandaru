import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import useCustomToast from '../utils/useCustomToast';
const CheckAuth = props => {
  const navigate = useNavigate();
  const isAuthenticated = localStorage.getItem('accessToken');
  const { ShowToast } = useCustomToast();

  console.log('cek auth');
  useEffect(() => {
    if (!isAuthenticated) {
      navigate('/signin');
      // ShowToast('Error', 'ntah error apa', 'error');
    } else {
      navigate('/admin/dashboard');
    }
  }, []);

  return props.children;
};

export default CheckAuth;
