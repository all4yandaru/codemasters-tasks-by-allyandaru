import { Box, Image } from '@chakra-ui/react';
import { useEffect } from 'react';
import useCustomToast from '../utils/useCustomToast';
// import showToast from '../utils/useCustomToast';

const UnderConstruction = () => {
  return (
    <Box
      w="100vw"
      h="100vh"
      p={4}
      color="white"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Image
        h="80vh"
        src={window.location.origin + '/under_construction.png'}
        alt="Dan Abramov"
      />
    </Box>
  );
};

export default UnderConstruction;
