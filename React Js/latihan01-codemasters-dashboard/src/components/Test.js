import { Box, Image } from '@chakra-ui/react';
import { useEffect } from 'react';
import useCustomToast from '../utils/useCustomToast';

const Test = () => {
  const { ShowToast } = useCustomToast();

  useEffect(() => {
    ShowToast('Error', 'ntah error apa', 'error');
  }, []);

  return (
    <Box
      w="100vw"
      h="100vh"
      p={4}
      color="white"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Image
        h="80vh"
        src={window.location.origin + '/under_maintenance.png'}
        alt="Dan Abramov"
      />
    </Box>
  );
};

export default Test;
