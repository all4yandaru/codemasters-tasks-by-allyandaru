import dayjs from 'dayjs';
import useCustomToast from '../../../utils/useCustomToast';
import Modal from '../../../components/Modal';
import { Flex } from '@chakra-ui/react';
import FormInput from '../../../components/FormInput';
import useStudent from '../../../services/useStudent';

const initialState = {
  name: '',
  email: '',
  phoneNumber: '',
  enrollNumber: '',
  admissionDate: '',
};

const ModalFormStudent = ({ data, onClose, isOpen, refresh }) => {
  const { ShowToast } = useCustomToast();
  const { addStudent, updateStudent } = useStudent();
  const [form, setForm] = useState(initialState);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (data) {
      setForm({ ...data });
    } else {
      setForm(initialState);
    }
  }, [isOpen]);

  const handleChange = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleAdd = async () => {
    setIsLoading(true);
    try {
      const res = await addStudent({
        ...form,
        admissionDate: dayjs(form.admissionDate).format('YYYY-MM-DD'),
      });
      ShowToast('Success', res?.message, 'success');
      onClose();
      refresh();
    } catch (e) {
      ShowToast('Error', e, 'error');
    }
    setIsLoading(false);
  };

  const handleUpdate = async () => {
    setIsLoading(true);
    try {
      const res = await updateStudent(data?.id, {
        ...form,
        admissionDate: dayjs(form.admissionDate).format('YYYY-MM-DD'),
      });
      ShowToast('Success', res?.message, 'success');
      onClose();
      refresh();
    } catch (e) {
      ShowToast('Error', e, 'error');
    }
    setIsLoading(false);
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      onConfirm={data ? handleUpdate : handleAdd}
      title={`${data ? 'Edit' : 'Add Data'}`}
      confirmButtonText="Save"
      isButtonLoading={isLoading}
    >
      <Flex gap="16px" direction="column">
        <FormInput
          name="name"
          value={form.name}
          onChange={handleChange}
          label="Name"
          placeholder="Karthi"
        />
        <FormInput
          name="email"
          value={form.email}
          onChange={handleChange}
          label="Email"
          placeholder="karthi@gmail.com"
        />
        <FormInput
          name="phoneNumber"
          value={form.phoneNumber}
          onChange={handleChange}
          label="Phone"
          placeholder="08xx"
        />
        <FormInput
          name="enrollNumber"
          value={form.enrollNumber}
          onChange={handleChange}
          readOnly
          label="Enroll Number"
          placeholder="123456"
        />
        <FormInput
          type="date"
          name="admissionDate"
          value={form.admissionDate}
          onChange={handleChange}
          readOnly
          label="Date of Admission"
          placeholder="08-Dec, 2023"
        />
      </Flex>
    </Modal>
  );
};

export default ModalFormStudent;
