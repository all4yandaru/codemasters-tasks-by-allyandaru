import React, { useState } from 'react';
import {
  ChakraProvider,
  Box,
  Text,
  Link,
  VStack,
  Code,
  Grid,
  theme,
} from '@chakra-ui/react';
import { AppRoutes } from './routes/AppRoutes';
import { ColorModeSwitcher } from './ColorModeSwitcher';
import { Logo } from './Logo';
import { Switch } from '@chakra-ui/react';
import '@fontsource/montserrat/400.css';
import '@fontsource/plus-jakarta-sans/700.css';

function App() {
  // const [isChecked, setisChecked] = useState(true);
  // console.log(isChecked);

  return (
    <ChakraProvider theme={theme}>
      <AppRoutes />

      {/* <Box textAlign="center" fontSize="xl">
        <Switch
          id="isChecked"
          isChecked={isChecked}
          onChange={() => setisChecked(!isChecked)}
        />

        <Grid minH="100vh" p={3}>
          <ColorModeSwitcher justifySelf="flex-end" />
          <VStack spacing={8}>
            <Logo h="40vmin" pointerEvents="none" />
            <Text>
              Edit <Code fontSize="xl">src/App.js</Code> and save to reload.
            </Text>
            <Link
              color="teal.500"
              href="https://chakra-ui.com"
              fontSize="2xl"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn Chakra
            </Link>
          </VStack>
        </Grid>
      </Box> */}
    </ChakraProvider>
  );
}

export default App;
