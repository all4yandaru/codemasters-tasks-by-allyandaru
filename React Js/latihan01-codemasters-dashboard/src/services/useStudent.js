import { services } from '.';

export default () => {
  const studentList = async (page = 1, q) => {
    try {
      const res = await services.get('students', {
        params: {
          page: page,
          search: q,
        },
      });
      return res;
    } catch (e) {
      throw e;
    }
  };

  const addStudent = async body => {
    try {
      const res = await services.post('student', body);
      return res;
    } catch (e) {
      throw e;
    }
  };

  return {
    addStudent,
    studentList,
  };
};
