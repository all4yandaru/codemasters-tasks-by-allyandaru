import { services } from '.';

export default () => {
  const signIn = async body => {
    try {
      console.log('res body ', body);
      const res = await services.post('auth', body);
      console.log('res login ', res);
      return res;
    } catch (e) {
      throw e;
    }
  };
  return {
    signIn,
  };
};
