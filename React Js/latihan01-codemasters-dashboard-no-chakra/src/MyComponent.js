import React from "react";
// import "./App.css";
import logo from "./assets/codemasters-logo.jpg";

const MyComponent = () => {
  return (
    <div className="myComponent">
      <div className="cardView">
        {/* TITLE ============================================================ */}
        <img src={logo} alt="logo" width={500} height={55} />
        <div style={{ marginTop: 32 }}>
          <h1 style={{ fontWeight: "normal", marginTop: 0, marginBottom: 0 }}>
            SIGN IN
          </h1>
        </div>
        <div>
          <h2 style={{ color: "gray", fontWeight: "normal", marginTop: 0 }}>
            Enter your credentials to access your account
          </h2>
        </div>

        {/* FORM ================================================================ */}
        <form className="inputForm">
          <h3 style={{ marginBottom: 8, fontWeight: "normal" }}>Email</h3>
          <input type="text" name="name" className="inputItem" />

          <h3 style={{ marginBottom: 8, fontWeight: "normal" }}>Password</h3>
          <input type="password" name="name" className="inputItem" />

          <input type="submit" value="Submit" className="submitButton" />
        </form>

        <p
          style={{
            marginBottom: 0,
            marginTop: 32,
            fontSize: 18,
            color: "gray",
          }}
        >
          forget your password?{" "}
          <a href="#" style={{ color: "red" }}>
            reset password
          </a>
        </p>
      </div>
    </div>
  );
};

export default MyComponent;
