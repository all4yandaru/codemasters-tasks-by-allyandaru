// common module
module.exports = {
  addNumbers: function (a, b) {
    return a + b;
  },
  subtractNumbers: (a, b) => a - b,
  moneyFormat: (amount) => `Rp. ${amount.toFixed(2)}`,
  dateFormat: (date) => date.toDateString(),
};

const myMath = require("./ownModule.js");
console.log(myMath.addNumbers(2, 5));
console.log(myMath.subtractNumbers(2, 5));
console.log(myMath.moneyFormat(12000.52323));
console.log(myMath.dateFormat(new Date()));
