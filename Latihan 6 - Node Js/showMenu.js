const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function showMenu() {
  console.log("Pilih Menu");
  console.log("1. Lihat profil");
  console.log("2. Ubah kata sandi");
  console.log("3. Keluar");
}

showMenu();

rl.question("Pilihan anda: ", (pilihan) => {
  switch (pilihan) {
    case "1":
      console.log("ubah profil here");
      break;
    case "2":
      console.log("ubah kata sandi here");
      break;
    case "3":
      console.log("keluar ya!");
      break;

    default:
      break;
  }
  rl.close();
});
