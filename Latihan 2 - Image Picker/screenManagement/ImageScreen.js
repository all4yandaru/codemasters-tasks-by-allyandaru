import {Button, Image, StyleSheet, Text, View} from 'react-native';
import React, {Component, useEffect, useState} from 'react';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {useDispatch, useSelector} from 'react-redux';

const ImageScreen = ({navigation, route}) => {
  const [selectedImage, setSelectedImage] = useState(null);
  const {image} = useSelector(state => state.image);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('image use state: ', image.image);
    if (image !== undefined) {
      setSelectedImage(image.image);
    }
  });

  const addImage = imageUri => {
    var imageLocation = {
      image: imageUri,
    };
    dispatch({type: 'IMAGE_ADD_DATA', data: imageLocation});
  };

  const openImagePicker = () => {
    console.log('image picker library');
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 2000,
      maxWidth: 2000,
    };

    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error: ', response.error);
      } else {
        let imageUri = response.uri || response.assets?.[0]?.uri;
        console.log('image uri: ', imageUri);
        addImage(imageUri);
      }
    });
  };

  const handleCameraLaunch = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 2000,
      maxWidth: 2000,
    };

    launchCamera(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled camera');
      } else if (response.error) {
        console.log('Camera Error: ', response.error);
      } else {
        // Process the captured image
        let imageUri = response.uri || response.assets?.[0]?.uri;
        setSelectedImage(imageUri);
        console.log(imageUri);
      }
    });
  };

  return (
    <View style={styles.safeAreaContent}>
      {selectedImage && (
        <Image
          source={{uri: selectedImage}}
          style={{flex: 1}}
          resizeMode="contain"
        />
      )}
      <View style={{marginTop: 20}}>
        <Button title="Choose from Device" onPress={openImagePicker} />
      </View>
      <View style={{marginTop: 20, marginBottom: 50}}>
        <Button title="Open Camera" onPress={handleCameraLaunch} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  safeAreaContent: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default ImageScreen;
