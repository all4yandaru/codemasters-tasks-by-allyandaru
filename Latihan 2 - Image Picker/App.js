import React from "react";
import { Provider } from "react-redux";
import { persistor, store } from "./stateManagement/utils/store";
import { PersistGate } from "redux-persist/integration/react";
import ImageScreen from "./screenManagement/ImageScreen";
import { SafeAreaView, StatusBar } from "react-native";

const App= () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'light-content'} />
          <ImageScreen/>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;
