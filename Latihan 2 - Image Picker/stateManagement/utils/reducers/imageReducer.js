const initialImageState = {
  image: '',
};

const imageReducer = (state = initialImageState, action) => {
  switch (action.type) {
    case 'IMAGE_ADD_DATA':
      console.log('add image: ', action.data);
      return {
        ...state,
        image: action.data,
      };

    default:
      return state;
  }
};

export default imageReducer;
