import {combineReducers} from 'redux';

const {default: imageReducer} = require('./reducers/imageReducer');

const rootReducer = combineReducers({
  image: imageReducer,
});

export default rootReducer;
