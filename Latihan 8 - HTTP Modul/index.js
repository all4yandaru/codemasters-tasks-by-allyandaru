const http = require("http");
const url = require("url");
const data = require("./books.json");

const server = http.createServer((req, res) => {
  var query = url.parse(req.url, true).query; // mengambil parameter ?keyword=Harry .....
  const urlSplit = req.url.split("/"); // split api berdasarkan garis miring
  const keyword = query?.keyword; // ambil parameter dengan nama "keyword"

  if (urlSplit.length == 2 && req.url.startsWith("/books")) {
    var dataToShow = data;
    if (keyword != undefined) {
      dataToShow = data.books.filter((value) => value.name.includes(keyword));
    }
    postData(res, dataToShow);
  } else if (urlSplit.length == 3 && urlSplit[1] == "books") {
    const searchData = data.books.filter((value) => value.id == urlSplit[2]);
    postData(res, searchData);
  } else {
    postData(res, { error: 500, description: "he kakehan request" });
  }
});

const postData = (res, param) => {
  res.writeHead(200, { "Content-Type": "application/json" });
  res.end(JSON.stringify(param));
};

const port = 3000;

server.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`);
});
