import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const App = () => {
  return (
    <SafeAreaView style={styles.parentContent}>
      <View style={[{backgroundColor: 'white'}, styles.sectionContainer]}>
        <Text style={styles.titleHeader}>Digital Approval</Text>
        <Image style={styles.imageStyle} source={require('./gambar.png')} />
        <TextInput
          style={styles.input}
          placeholder="Email"
          keyboardType="words"
        />
        <TextInput
          secureTextEntry={true}
          style={[styles.input, {marginTop: 8}]}
          placeholder="Password"
          keyboardType="words"
        />
        <Text style={{marginBottom: 24, alignSelf: 'flex-end'}}>
          Reset Password
        </Text>
        <TouchableOpacity style={styles.button}>
          <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: 'bold'}}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  parentContent: {
    backgroundColor: '#F7F7F7',
    justifyContent: 'center',
    flex: 1,
    padding: 20,
  },
  sectionContainer: {
    padding: 24,
    borderRadius: 10,
  },
  imageStyle: {
    marginBottom: 38,
    alignSelf: 'center',
  },
  input: {
    paddingHorizontal: 16,
    marginBottom: 16,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'gray',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#287AE5',
  },
  titleHeader: {
    backgroundColor: '#002558',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'center',
    paddingVertical: 16,
    paddingHorizontal: 40,
    borderRadius: 30,
    marginTop: -50,
    marginBottom: 27,
  },
});

export default App;
