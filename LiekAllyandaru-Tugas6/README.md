# JackFixer with Redux

materi:

- jackfixer UI
- redux

library:

- @react-navigation/bottom-tabs
- @react-navigation/native
- @react-navigation/native-stack
- @react-navigation/stack
- React-native-gesture-handler
- React-native-safe-area-context
- React-native-screens

- react-redux
- redux
- redux-persist
- Redux-thunk
- @react-native-async-storage/async-storage
