import accountReducer from './reducers/accountReducer';
import orderReducer from './reducers/orderReducer';
import sessionReducer from './reducers/sessionReducer';
import transactionReducer from './reducers/transactionReducer';

const {combineReducers} = require('redux');
const {default: storeReducer} = require('./reducers/storeReducer');

const rootReducer = combineReducers({
  store: storeReducer,
  order: orderReducer,
  transaction: transactionReducer,
  account: accountReducer,
  session: sessionReducer,
});

export default rootReducer;
