const storeDummy = [
  {
    id: 1,
    nama: 'Jack Repair Gejayan',
    alamat: 'Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384',
    buka: false,
    rating: 4.8,
    suka: true,
    jadwal: '09:00 - 21:00',
    deskripsi:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
    biaya: 'Rp 20.000 - 80.000',
    image: require('../../../assets/place_image_1.png'),
  },
  {
    id: 2,
    nama: 'Jack Repair Seturan',
    alamat: 'Jalan Seturan (Concat), 55281',
    buka: true,
    rating: 4.8,
    suka: false,
    jadwal: '09:00 - 21:00',
    deskripsi:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
    biaya: 'Rp 20.000 - 80.000',
    image: require('../../../assets/place_image_2.png'),
  },
];

const initialStoreState = {
  stores: storeDummy,
};

const storeReducer = (state = initialStoreState, action) => {
  console.log('store reducer: ', action);
  switch (action.type) {
    case 'STORE_ADD_DATA':
      return {
        ...state,
        stores: action.data,
      };

    default:
      return state;
  }
};

export default storeReducer;
