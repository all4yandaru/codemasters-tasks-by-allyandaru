import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {textStyle} from '../../assets/style/TextStyle';
import {useSelector} from 'react-redux';

const TransactionScreen = ({navigation}) => {
  const {trans} = useSelector(state => state.transaction);
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}>
        <FlatList
          data={trans}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('TransactionRouting', {
                  screen: 'DetailTransactionScreen',
                  params: item,
                })
              }
              style={[localStyle.cardView]}>
              <Text style={[textStyle.size12BlackNormal, {color: '#BDBDBD'}]}>
                {item.date}
              </Text>
              <Text style={[textStyle.size12BlackBold, {marginTop: 13}]}>
                {item.product.merek} - {item.product.warna} -{' '}
                {item.product.ukuran}
              </Text>
              <Text style={[textStyle.size12BlackNormal]}>
                {item.product.service.map(text => text.key).join(', ')}
              </Text>
              <View style={localStyle.rowContentBetween}>
                <Text style={[textStyle.size12BlackNormal]}>
                  Kode Reservasi :{' '}
                  <Text style={[textStyle.size12BlackBold]}>
                    C{item.id_trans.substring(0, 6)}
                  </Text>
                </Text>
                <Text
                  style={[
                    localStyle.transactionStatus,
                    {
                      backgroundColor: item.paid ? '#82D4BB' : '#F29C1F29',
                      color: item.paid ? '#246A55' : '#FFC107',
                    },
                  ]}>
                  {item.paid ? 'Paid' : 'Reserved'}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  cardView: {
    backgroundColor: 'white',
    margin: 12,
    borderRadius: 8,
    padding: 16,
    elevation: 4,
  },
  rowContentBetween: {
    flexDirection: 'row',
    marginTop: 13,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  transactionStatus: {
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 24,
  },
});
export default TransactionScreen;
