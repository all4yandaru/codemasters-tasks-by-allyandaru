import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component, useState} from 'react';
import {styles} from '../../assets/style/HomeStyle';
import {textStyle} from '../../assets/style/TextStyle';
import {useDispatch} from 'react-redux';

const CheckOutScreen = ({navigation, route}) => {
  console.log('check out params: ', route.params);
  const data = route.params;
  const totalPrice = data.product.service.length * 30 + 3;
  const dispatch = useDispatch();

  const [metodePembayaran, setMetodePembayaran] = useState([
    {
      metode: 'Bank Transfer',
      checked: false,
      direction: require('../../assets/cash.png'),
      id: 0,
    },
    {
      metode: 'OVO',
      checked: false,
      direction: require('../../assets/ovo.png'),
      id: 1,
    },
    {
      metode: 'Kartu Kredit',
      checked: false,
      direction: require('../../assets/cash.png'),
      id: 2,
    },
  ]);

  const [paymentChecked, setPaymentChecked] = useState(false);

  const changePaymentChecked = () => {
    var checkedMethod = metodePembayaran.find(value =>
      value.checked === true ? value.checked : false,
    );
    if (checkedMethod === undefined || checkedMethod === null) {
      console.log('method is not checked yet');
    } else {
      console.log('checked with ', checkedMethod.metode);
      updateTransactionStatus(true);
      navigation.navigate('MainRouting', {screen: 'Transaction'});
    }
  };

  const updateTransactionStatus = (status = false) => {
    var newData = data;
    newData.paid = status;
    console.log('new data : ', newData);
    dispatch({type: 'TRANS_UPDATE_DATA', data: newData});
  };

  const handleItemPress = item => {
    const updatedData = metodePembayaran.map(x =>
      x.id === item.id ? {...x, checked: !x.checked} : {...x, checked: false},
    );
    setMetodePembayaran(updatedData);
  };

  return (
    <View style={styles.safeAreaContent}>
      <ScrollView style={{flexGrow: 1, paddingBottom: 40}}>
        {/* Data Customer ================================================================== */}
        <View style={localStyle.sectionContent}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Data Customer
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            Agil Bani (082136564484)
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 8}]}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            gantengdoang@dipanggang.com
          </Text>
        </View>

        {/* ALAMAT TUJUAN ================================================================= */}
        <View style={localStyle.sectionContent}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Alamat Outlet Tujuan
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            {data.product.store.nama}
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 8}]}>
            {data.product.store.alamat}
          </Text>
        </View>

        {/* BARANG =========================================================================== */}
        <View style={localStyle.sectionContent}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Barang
          </Text>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <Image
              source={require('../../assets/shoes_image.png')}
              style={localStyle.cardImage}
            />
            <View
              style={{marginHorizontal: 13, justifyContent: 'center', flex: 1}}>
              <Text style={textStyle.size12BlackBold}>
                {data.product.merek} - {data.product.warna} -{' '}
                {data.product.ukuran}
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                {data.product.service.map(value => value.key).join(', ')}
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                Note : {data.product.note}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 16,
            }}>
            <Text style={textStyle.size16BlackNormal}>1 Pasang</Text>
            <Text style={textStyle.size16BlackBold}>@Rp 50.000</Text>
          </View>
        </View>

        {/* RINCIAN PEMBAYARAN ======================================================================= */}
        <View style={localStyle.sectionContent}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Rincian Pembayaran
          </Text>
          <FlatList
            data={data.product.service}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <View style={{flexDirection: 'row', marginTop: 11}}>
                <Text style={textStyle.size12BlackNormal}>{item.key}</Text>
                <Text
                  style={[
                    textStyle.size12BlackNormal,
                    {marginLeft: 15, color: '#FFC107'},
                  ]}>
                  x1 Pasang
                </Text>
                <Text
                  style={[textStyle.size12BlackNormal, {marginLeft: 'auto'}]}>
                  RP 30.000
                </Text>
              </View>
            )}
          />
          <View style={{flexDirection: 'row', marginTop: 11}}>
            <Text style={textStyle.size12BlackNormal}>Biaya Antar</Text>
            <Text style={[textStyle.size12BlackNormal, {marginLeft: 'auto'}]}>
              RP 3.000
            </Text>
          </View>
          <View
            style={{
              marginTop: 11,
              borderBottomColor: '#EDEDED',
              borderBottomWidth: StyleSheet.hairlineWidth,
            }}
          />
          <View style={{flexDirection: 'row', marginTop: 11}}>
            <Text style={textStyle.size12BlackNormal}>Total</Text>
            <Text style={[textStyle.size12BlackBold, {marginLeft: 'auto'}]}>
              RP {totalPrice.toLocaleString('en-FR')}.000
            </Text>
          </View>
        </View>
        {/* METODE PEMBAYARAN =========================================================== */}
        <View style={localStyle.sectionContent}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Pilih Pembayaran
          </Text>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <FlatList
              horizontal={true}
              contentContainerStyle={localStyle.list}
              data={metodePembayaran}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => handleItemPress(item)}
                  style={[
                    localStyle.pembayaran,
                    {backgroundColor: item.checked ? '#03426229' : 'white'},
                  ]}>
                  <Image
                    style={localStyle.pembayaranImage}
                    source={item.direction}
                  />
                  <Text style={{alignSelf: 'center', marginTop: 8}}>
                    {item.metode}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
        {/* TOMBOL PESAN ============================================================= */}
        <View style={{marginTop: 14, backgroundColor: 'white', padding: 20}}>
          <TouchableOpacity
            onPress={() =>
              // navigation.navigate('MainRouting', {screen: 'Transaction'})
              changePaymentChecked()
            }
            style={localStyle.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      {/* FOOTER ====================================================================== */}
    </View>
  );
};

const localStyle = StyleSheet.create({
  sectionContent: {
    paddingHorizontal: 24,
    paddingVertical: 18,
    marginTop: 12,
    backgroundColor: 'white',
  },
  buttonStyle: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
  cardImage: {
    width: 84,
    height: 84,
    resizeMode: 'contain',
    borderRadius: 8,
  },
  pembayaran: {
    width: 150,
    marginHorizontal: 10,
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 22,
    paddingVertical: 16,
    borderColor: '#E1E1E1',
  },
  pembayaranImage: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  list: {
    flexDirection: 'row',
  },
  footerContent: {
    backgroundColor: 'white',
    bottom: 0,
    paddingVertical: 22,
    paddingHorizontal: 20,
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
  },
});

export default CheckOutScreen;
