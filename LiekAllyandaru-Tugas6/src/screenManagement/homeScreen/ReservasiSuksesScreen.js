import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../../assets/style/HomeStyle';
import {textStyle} from '../../assets/style/TextStyle';

const ReservasiSuksesScreen = ({navigation}) => {
  return (
    <View style={[styles.safeAreaContent, {backgroundColor: 'white'}]}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MainRouting', {
              screen: 'Transaction',
            })
          }
          style={{alignSelf: 'baseline', margin: 24, position: 'absolute'}}>
          <Image
            style={{
              width: 24,
              height: 24,
            }}
            source={require('../../assets/close.png')}
          />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={[textStyle.size18BlackBold, {color: '#11A84E'}]}>
            Reservasi Berhasil
          </Text>
          <Image
            style={{width: 160, height: 160, marginVertical: 40}}
            source={require('../../assets/success.png')}
          />
          <Text style={([textStyle.size14BlackNormal], {textAlign: 'center'})}>
            Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
          </Text>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('MainRouting', {
                screen: 'Transaction',
              })
            }
            style={localStyle.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Lihat Kode Reservasi
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    marginTop: 24,
    padding: 19,
    borderRadius: 8,
  },
  footerContent: {
    bottom: 0,
    marginBottom: 48,
    paddingHorizontal: 20,
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
  },
});

export default ReservasiSuksesScreen;
