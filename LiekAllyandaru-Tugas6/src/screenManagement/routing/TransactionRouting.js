import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TransactionScreen from '../transactionScreen/TransactionScreen';
import DetailTransactionScreen from '../transactionScreen/DetailTransactionScreen';
import CheckOutScreen from '../transactionScreen/CheckOutScreen';

const Stack = createStackNavigator();
const TransactionRouting = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="TransactionScreen">
      <Stack.Screen
        name="TransactionScreen"
        component={TransactionScreen}
        options={{headerShown: true, title: 'Transaksi'}}
      />
      <Stack.Screen
        name="DetailTransactionScreen"
        component={DetailTransactionScreen}
        options={{headerShown: true, title: ' '}}
      />
      <Stack.Screen
        name="CheckOutScreen"
        component={CheckOutScreen}
        options={{headerShown: true, title: 'Checkout'}}
      />
    </Stack.Navigator>
  );
};

export default TransactionRouting;
