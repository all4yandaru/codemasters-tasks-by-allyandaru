import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SplashScreen from '../authScreen/SplashScreen';
import LoginScreen from '../authScreen/LoginScreen';
import RegisterScreen from '../authScreen/RegisterScreen';
import MainRouting from './MainRouting';
import HomeRouting from './HomeRouting';
import TransactionRouting from './TransactionRouting';

const Stack = createNativeStackNavigator();
const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="SplashScreen">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="MainRouting" component={MainRouting} />
        <Stack.Screen name="HomeRouting" component={HomeRouting} />
        <Stack.Screen
          name="TransactionRouting"
          component={TransactionRouting}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
