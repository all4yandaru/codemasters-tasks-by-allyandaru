import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileScreen from '../profileScreen/profileScreen';

const Stack = createStackNavigator();

const ProfileRouting = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="ProfileScreen">
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      {/* <Stack.Screen
        name="EditProfileScreen"
        component={EditProfileScreen}
        options={{headerShown: true, title: 'Edit Profile'}}
      />
      <Stack.Screen
        name="FAQScreen"
        component={FAQScreen}
        options={{headerShown: true, title: 'Edit Profile'}} 
      />*/}
    </Stack.Navigator>
  );
};

export default ProfileRouting;
