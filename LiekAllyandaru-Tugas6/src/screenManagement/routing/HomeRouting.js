import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../homeScreen/HomeScreen';
import DetailScreen from '../homeScreen/DetailScreen';
import FormulisPemesanan from '../homeScreen/FormulirPemesanan';
import KeranjangScreen from '../homeScreen/KeranjangScreen';
import SummaryScreen from '../homeScreen/SummaryScreen';
import ReservasiSuksesScreen from '../homeScreen/ReservasiSuksesScreen';

const Stack = createStackNavigator();
const HomeRouting = ({navigation}) => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomeScreen">
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="DetailScreen" component={DetailScreen} />
      <Stack.Screen
        name="PemesananScreen"
        component={FormulisPemesanan}
        options={{headerShown: true, title: 'Formulir Pemesanan'}}
      />
      <Stack.Screen
        name="KeranjangScreen"
        component={KeranjangScreen}
        options={{headerShown: true, title: 'Keranjang'}}
      />
      <Stack.Screen
        name="SummaryScreen"
        component={SummaryScreen}
        options={{headerShown: true, title: 'Summary'}}
      />
      <Stack.Screen
        name="ReservasiSuksesScreen"
        component={ReservasiSuksesScreen}
      />
    </Stack.Navigator>
  );
};

export default HomeRouting;
