import {
  Image,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
  VirtualizedList,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../../assets/style/AuthStyles';
import {ScrollView} from 'react-native-gesture-handler';
import {Base64} from 'js-base64';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

const RegisterScreen = ({navigation}) => {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const {account} = useSelector(state => state.account);

  const dispatch = useDispatch();

  const registerAccount = () => {
    const encrypedtPassword = Base64.encode(password);
    const emailValidation = validate(email);
    const isPasswordCorrect = password === confirmPassword ? true : false;

    if (emailValidation && isPasswordCorrect) {
      // add account
      const newAccount = {
        email: email,
        nama: nama,
        password: encrypedtPassword,
      };
      // check account
      console.log('all account: ', account);
      if (account === undefined) {
        addAccount(newAccount);
        ToastAndroid.show('Pendaftaran berhasil!', ToastAndroid.SHORT);
      } else {
        if (checkAccount(newAccount)) {
          ToastAndroid.show(
            'email sudah ada, silahan ganti email anda!',
            ToastAndroid.SHORT,
          );
        } else {
          addAccount(newAccount);
          ToastAndroid.show('Pendaftaran berhasil!', ToastAndroid.SHORT);
          navigation.goBack();
        }
      }
    } else {
      ToastAndroid.show('email atau password salah!', ToastAndroid.SHORT);
    }
  };

  const addAccount = data => {
    var newAccount = [...account];
    newAccount.push(data);
    dispatch({type: 'AUTH_ADD_DATA', data: newAccount});
  };

  const checkAccount = data => {
    const checkAcc = account.find(value => value.email == data.email);
    console.log('acc:', checkAcc);
    if (checkAcc === undefined) {
      return false;
    } else {
      return true;
    }
  };

  const validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(text) === false) {
      console.log('Email is Not Correct');
      return false;
    } else {
      console.log('Email is Correct');
      return true;
    }
  };

  return (
    <View style={styles.safeAreaContent}>
      {/* MAIN CONTENT ============================================================================ */}
      <ScrollView style={styles.scrollViewContent}>
        <Image
          style={styles.splashImage}
          source={require('../../assets/smile.png')}
        />

        {/* FORM */}
        <View style={styles.formContent}>
          <Text style={styles.titleText}>Welcome,{'\n'}Please Register</Text>

          <Text style={[styles.labelFormText, {marginTop: 25}]}>Nama</Text>
          <TextInput
            style={styles.textInputStyle}
            onChangeText={value => setNama(value)}
            value={nama}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Enter your name"
          />

          <Text style={[styles.labelFormText, {marginTop: 25}]}>Email</Text>
          <TextInput
            onChangeText={value => setEmail(value)}
            value={email}
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="youremail@gmail.com"
            keyboardType="email-address"
          />

          <Text style={[styles.labelFormText, {marginTop: 25}]}>Password</Text>
          <TextInput
            onChangeText={value => setPassword(value)}
            value={password}
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Enter password"
            secureTextEntry={true}
          />

          <Text style={[styles.labelFormText, {marginTop: 25}]}>
            Confirm Password
          </Text>
          <TextInput
            onChangeText={value => setConfirmPassword(value)}
            value={confirmPassword}
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Re-enter password"
            secureTextEntry={true}
          />

          {/* LOGIN METHOD */}
          <View style={styles.loginMethodContent}>
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/gmail.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/facebook.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../../assets/twitter.png')}
            />
          </View>

          {/* BUTTON */}
          <TouchableOpacity
            onPress={() => registerAccount()}
            style={[styles.buttonStyle, {marginTop: 40}]}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Register
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      {/* FOOTER ================================================================================= */}
      <View style={styles.footerContent}>
        <Text>
          Don't Have An Account?{' '}
          <Text style={{fontWeight: '500', color: '#BB2427'}}>Register</Text>
        </Text>
      </View>
    </View>
  );
};

export default RegisterScreen;
