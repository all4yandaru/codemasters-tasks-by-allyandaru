import {Image, View} from 'react-native';
import React, {Component, useEffect} from 'react';
import {styles} from '../../assets/style/AuthStyles';
import {useSelector} from 'react-redux';

const SplashScreen = ({navigation}) => {
  const {session} = useSelector(state => state.session);
  useEffect(() => {
    setTimeout(() => {
      if (session.nama && session.email) {
        navigation.replace('MainRouting');
      } else {
        navigation.navigate('LoginScreen');
      }
    }, 1000);
  });

  return (
    <View style={styles.safeAreaContent}>
      <View style={styles.splashContent}>
        <Image
          style={[styles.splashImage, {width: 230, height: 238}]}
          source={require('../../assets/jack_fixer_logo.png')}
        />
      </View>
    </View>
  );
};

export default SplashScreen;
