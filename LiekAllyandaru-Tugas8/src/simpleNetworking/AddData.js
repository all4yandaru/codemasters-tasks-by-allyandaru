import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';
import axios from 'axios';

const AddData = ({modalVisible, onCloseModal, dataModal, setDataModal}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  const headers = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: TOKEN,
    },
  };

  useEffect(() => {
    if (dataModal) {
      const data = dataModal;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    } else {
      resetData();
    }
  }, [modalVisible]);

  const resetData = () => {
    setNamaMobil('');
    setTotalKM('');
    setHargaMobil('');
  };

  const postData = async () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    axios
      .post(`${BASE_URL}mobil`, body, headers) // wajib body dlu baru header
      .then(response => {
        console.log('response success: ', response);
        if (response.status === 200 || response.status === 201) {
          Alert.alert('Data Mobil berhasil ditambahkan');
          resetData();
          onCloseModal();
        }
      })
      .catch(error => console.log('error add data: ', error));
  };

  const editData = async () => {
    const body = [
      {
        _uuid: dataModal._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    axios
      .put(`${BASE_URL}mobil`, body, headers)
      .then(response => {
        if (response.status === 200 || response.status === 201) {
          Alert.alert('Data Mobil berhasil dirubah');
          resetData();
          onCloseModal();
        }
      })
      .catch(error => console.log('error update data: ', error));
  };

  const deleteData = async () => {
    const body = [
      {
        _uuid: dataModal._uuid,
      },
    ];

    axios
      .delete(`${BASE_URL}mobil`, {data: body, ...headers})
      .then(response => {
        if (response.status === 200 || response.status === 201) {
          Alert.alert('Data Mobil berhasil dihapus');
          resetData();
          onCloseModal();
        }
      })
      .catch(error => console.log('error delete data: ', error));
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onCloseModal();
        setDataModal(null);
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            onCloseModal(false);
            setDataModal(null);
          }}
          style={{
            height: '100%',
            width: '100%',
            position: 'absolute',
            backgroundColor: 'black',
            opacity: 0.2,
          }}></TouchableOpacity>
        {/* ADD DATA */}
        <View
          style={{
            backgroundColor: 'white',
            margin: 16,
            padding: 8,
            paddingTop: 16,
            borderRadius: 16,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                color: '#000',
                marginLeft: 10,
              }}>
              {dataModal ? 'Update Data' : 'Tambah Data'}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              padding: 15,
            }}>
            <View>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Nama Mobil
              </Text>
              <TextInput
                onChangeText={text => setNamaMobil(text)}
                value={namaMobil}
                placeholder="Masukkan Nama Mobil"
                style={styles.txtInput}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Total Kilometer
              </Text>
              <TextInput
                onChangeText={text => setTotalKM(text)}
                value={totalKM}
                placeholder="contoh: 100 KM"
                style={styles.txtInput}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Harga Mobil
              </Text>
              <TextInput
                onChangeText={text => setHargaMobil(text)}
                value={hargaMobil}
                placeholder="Masukkan Harga Mobil"
                style={styles.txtInput}
                keyboardType="number-pad"
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                dataModal ? editData() : postData();
              }}
              style={styles.btnAdd}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                {dataModal ? 'Update Data' : 'Tambah Data'}
              </Text>
            </TouchableOpacity>
            {dataModal && (
              <TouchableOpacity
                onPress={() => deleteData()}
                style={[styles.btnAdd, {backgroundColor: 'red'}]}>
                <Text style={{color: '#fff', fontWeight: '600'}}>
                  Hapus Data
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
