import {
  Alert,
  FlatList,
  Image,
  Modal,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component, useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import AddData from './AddData';
import axios from 'axios';

const Home = ({navigation, route}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const isFocused = useIsFocused();

  const [dataMobil, setDataMobil] = useState([]);
  const [datamodal, setDatamodal] = useState(null);

  useEffect(() => {
    getDataMobil();
    setIsLoading(true);
    console.log('is focused: ', isFocused);
  }, [isFocused]);

  const getDataMobil = async () => {
    console.log('Getting Data Mobil');
    axios
      .get(`${BASE_URL}mobil`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
      })
      .then(response => {
        console.log('res get mobil', response);

        if (response.status === 200) {
          const result = response.data.items;
          console.log('response JSON: ', result);
          setDataMobil(result);
          setIsLoading(false);
        } else if (response.status === 401 || response.status === 402) {
          return false;
        } else if (response.status === 500) {
          return false;
        }
      })
      .catch(error => {
        console.log('error get mobil', error);
      });
  };

  const showLoading = () => {
    return (
      <Modal animationType="fade" transparent={true} visible={isLoading}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 16,
              backgroundColor: 'white',
              borderRadius: 32,
              elevation: 2,
            }}>
            <Image
              source={require('../assets/lottielego.gif')}
              style={{width: 100, height: 100}}
            />
          </View>
        </View>
      </Modal>
    );
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      {/* MODAL POP UP ============================================================ */}
      {showLoading()}
      {/* {showAdd()} */}
      <AddData
        modalVisible={modalVisible}
        onCloseModal={() => {
          setModalVisible(false);
          getDataMobil();
        }}
        dataModal={datamodal}
        setDataModal={setDatamodal}
      />

      {/* LIST ITEM =================================================================================== */}
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => {
              // navigation.navigate('AddData', item)
              setModalVisible(true);
              setDatamodal(item);
            }}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {'Rp '}
                  {convertCurrency(item.harga)}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />

      {/* PLUS BUTTON ======================================================================== */}
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => {
          setModalVisible(true);
          setDatamodal(null);
        }}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  safeAreaContent: {
    flex: 1,
    backgroundColor: 'red',
  },
});

export default Home;
