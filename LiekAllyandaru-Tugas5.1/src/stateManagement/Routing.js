import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Login from './Login';
import Home from './Home';

const Stack = createStackNavigator();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: true, title: 'Login'}}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: true, title: 'Home'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
