const express = require("express");
const BookValidator = require("../supports/validator/BookValidator");
const { Book } = require("../models");
const router = express.Router();

router.get("/", (req, res) => {
  Book.findAll({ include: "Author" }).then((books) => {
    res.json(books);
  });
});

router.get("/:id", (req, res) => {
  Book.findByPk(req.params.id, { include: "Author" }).then((book) => {
    res.json(book);
  });
});

router.post("/", new BookValidator().validate(), async (req, res) => {
  const newData = await Book.create(req.body).then(
    res.json({ status: 200, message: "insert success" })
  );
});

module.exports = router;
