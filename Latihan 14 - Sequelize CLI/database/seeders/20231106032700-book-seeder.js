"use strict";

const { faker } = require("@faker-js/faker");

const bookArray = [];
for (let index = 0; index < 10; index++) {
  bookArray.push({
    title: faker.commerce.productName(),
    isbn: faker.commerce.isbn(),
    authorId: Math.floor(Math.random() * 10),
    createdAt: new Date(),
    updatedAt: new Date(),
  });
}

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("books", bookArray, {});
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("books", null, {});
  },
};
