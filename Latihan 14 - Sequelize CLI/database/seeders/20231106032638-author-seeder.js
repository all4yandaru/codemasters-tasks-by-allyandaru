"use strict";

const { faker } = require("@faker-js/faker");

const authorArray = [];
for (let index = 0; index < 10; index++) {
  authorArray.push({
    name: faker.company.name(),
    createdAt: new Date(),
    updatedAt: new Date(),
  });
}

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("authors", authorArray, {});
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("authors", null, {});
  },
};
