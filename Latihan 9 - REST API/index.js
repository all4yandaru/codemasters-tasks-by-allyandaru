const http = require("http");

const products = [
  { id: 1, name: "Produk A" },
  { id: 2, name: "Produk B" },
  { id: 3, name: "Produk C" },
];

const isDataIdExist = (id) => {
  return products.find((val) => val.id == id) ? true : false;
};

const server = http.createServer();

server.on("request", (req, res) => {
  // GET METHOD ==========================================================
  if (req.method === "GET" && req.url === "/api/products") {
    sendResponse(res, 200, products);
  }

  // POST METHOD ==========================================================
  if (req.method === "POST" && req.url === "/api/products") {
    let data = "";

    req.on("data", (chunk) => {
      data += chunk;
    });

    req.on("end", () => {
      if (!data) {
        sendError(res, 422, "Unprocessable Entity");
      } else {
        const newProduct = JSON.parse(data);
        console.log("has body: ", newProduct.name ? true : false);
        if (isDataIdExist(newProduct.id)) {
          sendError(res, 422, "Unprocessable Entity");
        } else {
          products.push(newProduct);
          sendResponse(res, 201, products);
        }
      }
    });
  }

  // PUT METHOD ==========================================================
  if (req.method === "PUT" && req.url.startsWith("/api/products/")) {
    const productId = parseInt(req.url.split("/").pop());

    let data = "";

    req.on("data", (chunk) => {
      data += chunk;
    });

    req.on("end", () => {
      const updatedData = JSON.parse(data);

      if (isDataIdExist(productId)) {
        const searchIndex = products.findIndex((val) => val.id == productId);
        products[searchIndex].name = updatedData.name;
        sendResponse(res, 200, products);
      } else {
        sendError(res, 422, "Unprocessable Entity");
      }
    });
  }

  if (req.method === "DELETE" && req.url.startsWith("/api/products/")) {
    const productId = parseInt(req.url.split("/").pop());

    if (isDataIdExist(productId)) {
      const index = products.findIndex((val) => val.id == productId);
      const newData = products.splice(index, 1);
      sendResponse(res, 200, products);
    } else {
      sendError(res, 422, "Unprocessable Entity");
    }
  }
});

const sendResponse = (res, status = 200, body) => {
  res.writeHead(status, { "Content-Type": "application/json" });
  res.end(JSON.stringify(body));
};

const sendError = (res, status = 500, message) => {
  res.writeHead(status, { "Content-Type": "application/json" });
  res.end(JSON.stringify({ status: status, message: message }));
};

const port = 3000;

server.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`);
});
