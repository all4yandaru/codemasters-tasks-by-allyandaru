import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './screen/HomeScreen';
import DetailScreen from './screen/DetailScreen';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import FormulisPemesanan from './screen/FormulisPemesanan';
import KeranjangScreen from './screen/KeranjangScreen';
import SummaryScreen from './screen/SummaryScreen';
import ReservasiSuksesScreen from './screen/ReservasiSuksesScreen';

const Stack = createStackNavigator();
const HomeRouting = ({navigation}) => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomeScreen">
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="DetailScreen" component={DetailScreen} />
      <Stack.Screen
        name="PemesananScreen"
        component={FormulisPemesanan}
        options={{headerShown: true, title: 'Formulir Pemesanan'}}
      />
      <Stack.Screen
        name="KeranjangScreen"
        component={KeranjangScreen}
        options={{headerShown: true, title: 'Keranjang'}}
      />
      <Stack.Screen
        name="SummaryScreen"
        component={SummaryScreen}
        options={{headerShown: true, title: 'Summary'}}
      />
      <Stack.Screen
        name="ReservasiSuksesScreen"
        component={ReservasiSuksesScreen}
      />
    </Stack.Navigator>
  );
};

export default HomeRouting;
