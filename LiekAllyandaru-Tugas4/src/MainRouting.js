import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ProfileScreen from './screen/ProfileScreen';
import MyTabBar from './assets/MyTabBar';
import TransactionScreen from './screen/TransactionScreen';
import HomeRouting from './HomeRouting';
import TransactionRouting from './TransactionRouting';
import ProfileRouting from './ProfileRouting';

const Tab = createBottomTabNavigator();
const MainRouting = () => {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={HomeRouting} />
      <Tab.Screen name="Transaction" component={TransactionRouting} />
      <Tab.Screen name="Profile" component={ProfileRouting} />
    </Tab.Navigator>
  );
};

export default MainRouting;
