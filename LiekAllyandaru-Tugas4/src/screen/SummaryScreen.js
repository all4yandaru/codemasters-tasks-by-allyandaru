import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {styles} from '../assets/style/HomeStyle';
import {textStyle} from '../assets/style/TextStyle';

const SummaryScreen = ({navigation}) => {
  return (
    <View style={styles.safeAreaContent}>
      <ScrollView style={{flexGrow: 1}}>
        {/* Data Customer ================================================================== */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Data Customer
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            Agil Bani (082136564484)
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 8}]}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            gantengdoang@dipanggang.com
          </Text>
        </View>

        {/* ALAMAT TUJUAN ================================================================= */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Alamat Outlet Tujuan
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 10}]}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 8}]}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>

        {/* BARANG =========================================================================== */}
        <View
          style={{
            paddingHorizontal: 24,
            paddingVertical: 18,
            marginTop: 12,
            backgroundColor: 'white',
          }}>
          <Text style={[textStyle.size14BlackNormal, {color: '#979797'}]}>
            Barang
          </Text>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <Image
              source={require('../assets/shoes_image.png')}
              style={localStyle.cardImage}
            />
            <View style={{marginHorizontal: 13, justifyContent: 'center'}}>
              <Text style={textStyle.size12BlackBold}>
                New Balance - Pink Abu - 40
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                Cuci Sepatu
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                Note : -
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      {/* FOOTER ====================================================================== */}
      <View style={localStyle.footerContent}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ReservasiSuksesScreen')}
          style={localStyle.buttonStyle}>
          <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const localStyle = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
  cardImage: {
    width: 84,
    height: 84,
    resizeMode: 'contain',
    borderRadius: 8,
  },
  footerContent: {
    bottom: 0,
    marginBottom: 48,
    paddingHorizontal: 20,
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
  },
});

export default SummaryScreen;
