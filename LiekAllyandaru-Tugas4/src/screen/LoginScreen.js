import {
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  VirtualizedList,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../assets/style/AuthStyles';
import {ScrollView} from 'react-native-gesture-handler';

const LoginScreen = ({navigation}) => {
  return (
    <View style={styles.safeAreaContent}>
      {/* MAIN CONTENT ============================================================================ */}
      <ScrollView style={styles.scrollViewContent}>
        <Image
          style={styles.splashImage}
          source={require('../assets/smile.png')}
        />

        {/* FORM */}
        <View style={styles.formContent}>
          <Text style={styles.titleText}>Welcome,{'\n'}Please Login First</Text>

          <Text style={[styles.labelFormText, {marginTop: 25}]}>Email</Text>
          <TextInput
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="youremail@gmail.com"
            keyboardType="email-address"
          />

          <Text style={[styles.labelFormText, {marginTop: 8}]}>Password</Text>
          <TextInput
            style={styles.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Password***"
            secureTextEntry={true}
          />

          {/* LOGIN METHOD */}
          <View style={styles.loginMethodContent}>
            <Image
              style={{marginRight: 18}}
              source={require('../assets/gmail.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../assets/facebook.png')}
            />
            <Image
              style={{marginRight: 18}}
              source={require('../assets/twitter.png')}
            />
            <Text
              style={{
                alignSelf: 'center',
                marginLeft: 'auto',
                color: '#717171',
              }}>
              Forgort Password?
            </Text>
          </View>

          {/* BUTTON */}
          <TouchableOpacity
            onPress={() => navigation.navigate('MainRouting')}
            style={styles.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      {/* FOOTER ================================================================================= */}
      <View style={styles.footerContent}>
        <Text>
          Don't Have An Account?{' '}
          <Text
            onPress={() => {
              navigation.navigate('RegisterScreen');
            }}
            style={{fontWeight: '500', color: '#BB2427'}}>
            Register
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default LoginScreen;
