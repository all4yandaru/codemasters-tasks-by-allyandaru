import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../assets/style/HomeStyle';

const EditProfileScreen = ({navigation}) => {
  const [Nama, onChangeNama] = React.useState('Agil Bani');
  const [Email, onChangeEmail] = React.useState('gilagil@gmail.com');
  const [Phone, onChangePhone] = React.useState('08124564879');
  return (
    <View style={[styles.safeAreaContent, {backgroundColor: 'white'}]}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Image
          style={localStyle.profilePict}
          source={require('../assets/profile_pict.png')}
        />
        <View
          style={{
            flexDirection: 'row',
            marginTop: 18,
            alignSelf: 'center',
          }}>
          <Image
            style={localStyle.icon}
            source={require('../assets/edit.png')}
          />
          <Text
            style={{
              fontSize: 18,
              color: '#3A4BE0',
              fontWeight: '500',
              marginLeft: 8,
            }}>
            Edit Foto
          </Text>
        </View>
        <View style={{marginTop: 55, paddingHorizontal: 21}}>
          <Text style={[localStyle.labelFormText]}>Nama</Text>
          <TextInput
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="Nama Lengkap"
            value={Nama}
            onChangeText={onChangeNama}
          />
          <Text style={[localStyle.labelFormText, {marginTop: 27}]}>Email</Text>
          <TextInput
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            keyboardType="email-address"
            placeholder="Email@gmail.com"
            value={Email}
            onChangeText={onChangeEmail}
          />
          <Text style={[localStyle.labelFormText, {marginTop: 27}]}>No hp</Text>
          <TextInput
            style={localStyle.textInputStyle}
            placeholderTextColor={'#C0C8E7'}
            placeholder="0821 3636 4542"
            keyboardType="phone-pad"
            value={Phone}
            onChangeText={onChangePhone}
          />

          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={localStyle.buttonStyle}>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
              Simpan
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  profilePict: {
    width: 95,
    height: 95,
    marginTop: 32,
    alignSelf: 'center',
  },
  icon: {
    width: 24,
    height: 24,
  },
  labelFormText: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  textInputStyle: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7,
    paddingHorizontal: 11,
    paddingVertical: 14,
    marginTop: 11,
  },
  buttonStyle: {
    backgroundColor: '#BB2427',
    marginTop: 76,
    marginBottom: 24,
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
});

export default EditProfileScreen;
