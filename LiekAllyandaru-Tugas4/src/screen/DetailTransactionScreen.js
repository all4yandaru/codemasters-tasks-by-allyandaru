import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {textStyle} from '../assets/style/TextStyle';

const DetailTransactionScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}>
        {/* KODE ====================================================== */}
        <View
          style={{
            backgroundColor: 'white',
            paddingVertical: 24,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={[textStyle.size12BlackNormal, {color: '#BDBDBD'}]}>
            20 Desember 2020 09:00
          </Text>
          <Text style={[textStyle.size36BlackBold, {marginTop: 54}]}>
            CS122001
          </Text>
          <Text style={[textStyle.size14BlackNormal, {marginTop: 8}]}>
            Kode Reservasi
          </Text>
          <Text
            style={[
              textStyle.size14BlackNormal,
              {marginTop: 42, color: '#6F6F6F'},
            ]}>
            Sebutkan Kode Reservasi saat tiba di outlet{' '}
          </Text>
        </View>
        {/* BARANG ==================================================================== */}
        <View style={{marginTop: 12, marginHorizontal: 12}}>
          <Text style={textStyle.size14BlackNormal}>Barang</Text>
          <View style={localStyle.cardView}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../assets/shoes_image.png')}
                style={localStyle.cardImage}
              />
              <View
                style={{marginHorizontal: 13, justifyContent: 'space-evenly'}}>
                <Text style={textStyle.size12BlackBold}>
                  New Balance - Pink Abu - 40
                </Text>
                <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                  Cuci Sepatu
                </Text>
                <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                  Note : -
                </Text>
              </View>
            </View>
          </View>
        </View>
        {/* STATUS PESANAN ================================================================= */}
        <View style={{marginTop: 36, marginHorizontal: 12}}>
          <Text style={textStyle.size14BlackNormal}>Status Pesanan</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('CheckOutScreen')}
            style={localStyle.cardView}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
              }}>
              <Image
                style={{width: 14, height: 14}}
                source={require('../assets/red_dot.png')}
              />
              <View style={{marginHorizontal: 13}}>
                <Text style={textStyle.size12BlackBold}>Telah Reservasi </Text>
                <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                  20 Desember 2020{' '}
                </Text>
              </View>
              <Text style={{marginLeft: 'auto'}}>09:00</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  cardView: {
    backgroundColor: 'white',
    paddingHorizontal: 14,
    paddingVertical: 24,
    borderRadius: 8,
    elevation: 4,
    marginTop: 16,
  },
  cardImage: {
    width: 84,
    height: 84,
    resizeMode: 'contain',
    borderRadius: 8,
  },
});

export default DetailTransactionScreen;
