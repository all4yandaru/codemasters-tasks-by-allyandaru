import {Image, Text, View} from 'react-native';
import React, {Component, useEffect} from 'react';
import {styles} from '../assets/style/AuthStyles';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('LoginScreen');
    }, 1000);
  });

  return (
    <View style={styles.safeAreaContent}>
      <View style={styles.splashContent}>
        <Image
          style={[styles.splashImage, {width: 230, height: 238}]}
          source={require('../assets/jack_fixer_logo.png')}
        />
      </View>
    </View>
  );
};

export default SplashScreen;
