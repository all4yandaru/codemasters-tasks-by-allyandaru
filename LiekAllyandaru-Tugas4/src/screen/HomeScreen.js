import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../assets/style/HomeStyle';

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.safeAreaContent}>
      <ScrollView style={{flexGrow: 1}}>
        {/* TOP CONTENT =================================================================== */}
        <View style={styles.topContent}>
          <View style={styles.rowSpaceBetween}>
            <Image
              style={styles.profileImageSmall}
              source={require('../assets/profile_pict.png')}
            />
            <Image
              style={[styles.iconSmall, {height: 20}]}
              source={require('../assets/bag_icon.png')}
            />
          </View>
          <Text
            style={[
              styles.textSubTitle,
              {
                color: '#034262',
                alignSelf: 'baseline',
                paddingVertical: 10,
              },
            ]}>
            Hello, Agil!
          </Text>
          <Text style={[styles.textTitle, {color: '#0A0827'}]}>
            Ingin merawat dan memperbaiki sepatumu? cari disini
          </Text>

          <View style={{flexDirection: 'row', marginTop: 20}}>
            <View style={[styles.textInputImageForm]}>
              <Image
                style={[styles.iconSmall, {margin: 12}]}
                source={require('../assets/search_icon.png')}
              />
              <TextInput style={{flex: 1}} />
            </View>

            <TouchableOpacity style={styles.imageButton}>
              <Image
                style={[styles.iconSmall, {margin: 12}]}
                source={require('../assets/filter_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        {/* CATEGORY CONTENT ============================================================== */}
        <View style={styles.categoryContent}>
          <TouchableOpacity style={styles.categoryButton}>
            <Image
              style={styles.categoryImage}
              source={require('../assets/shoes_icon.png')}
            />
            <Text>Sepatu</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.categoryButton}>
            <Image
              style={styles.categoryImage}
              source={require('../assets/jacket_icon.png')}
            />
            <Text>Jaket</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.categoryButton}>
            <Image
              style={styles.categoryImage}
              source={require('../assets/tas_icon.png')}
            />
            <Text>Tas</Text>
          </TouchableOpacity>
        </View>

        {/* LIST ITEM CONTENT ============================================================= */}
        <View style={styles.itemContent}>
          <View style={[styles.rowSpaceBetween, {marginBottom: 24}]}>
            <Text
              style={[styles.textDesc, {fontWeight: '600', color: 'black'}]}>
              Rekomendasi Terdekat
            </Text>
            <Text style={[{fontSize: 10, fontWeight: '600', color: '#E64C3C'}]}>
              View All
            </Text>
          </View>

          <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeRouting', {screen: 'DetailScreen'})
            }>
            <View style={styles.cardContent}>
              <Image
                style={styles.cardImage}
                source={require('../assets/place_image_1.png')}
              />
              <View style={{flex: 1}}>
                <View style={styles.rowSpaceBetween}>
                  <View>
                    <Image
                      style={{width: 50, height: 8, resizeMode: 'contain'}}
                      source={require('../assets/star_icon.png')}
                    />
                    <Text style={{color: '#D8D8D8'}}>4.8 Rating</Text>
                  </View>
                  <Image
                    style={{
                      width: 12,
                      height: 13,
                      resizeMode: 'contain',
                      alignSelf: 'flex-start',
                    }}
                    source={require('../assets/hearth_red_icon.png')}
                  />
                </View>
                <Text style={styles.textLocationName}>Jack Repair Gejayan</Text>
                <Text style={styles.textLocationDesc}>
                  Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                </Text>
                <Text style={styles.textClosed}>TUTUP</Text>
              </View>
            </View>
          </TouchableOpacity>
          {/* card content#2 */}
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeRouting', {screen: 'DetailScreen'})
            }>
            <View style={styles.cardContent}>
              <Image
                style={styles.cardImage}
                source={require('../assets/place_image_2.png')}
              />
              <View style={{flex: 1}}>
                <View style={styles.rowSpaceBetween}>
                  <View>
                    <Image
                      style={{width: 50, height: 8, resizeMode: 'contain'}}
                      source={require('../assets/star_icon.png')}
                    />
                    <Text style={{color: '#D8D8D8'}}>4.7 Rating</Text>
                  </View>
                  <Image
                    style={{
                      width: 12,
                      height: 13,
                      resizeMode: 'contain',
                      alignSelf: 'flex-start',
                    }}
                    source={require('../assets/hearth_gray_icon.png')}
                  />
                </View>
                <Text style={styles.textLocationName}>Jack Repair Seturan</Text>
                <Text style={styles.textLocationDesc}>
                  Jl. Seturan Kec. Laweyan . . .
                </Text>
                <Text style={styles.textOpen}>BUKA</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
