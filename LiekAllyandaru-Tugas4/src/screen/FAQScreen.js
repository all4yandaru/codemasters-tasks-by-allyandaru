import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React, {Component, useState} from 'react';
import {styles} from '../assets/style/HomeStyle';

const FAQScreen = () => {
  const [isSelected, setSelected] = useState(false);

  return (
    <View style={styles.safeAreaContent}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          paddingHorizontal: 8,
          paddingVertical: 16,
        }}>
        <View
          style={{
            backgroundColor: 'white',
            padding: 16,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{flex: 1, color: '#000000', fontSize: 14}}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit?
            </Text>
            <View>
              <TouchableOpacity
                style={{alignSelf: 'flex-start'}}
                onPress={() => setSelected(!isSelected)}>
                <Image
                  style={{width: 24, height: 24}}
                  source={
                    isSelected
                      ? require('../assets/chevron_down.png')
                      : require('../assets/chevron_up.png')
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
          {isSelected ? (
            <View style={{flexDirection: 'row', marginTop: 8}}>
              <Text style={{flex: 1, color: '#595959', fontSize: 14}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit?
              </Text>
            </View>
          ) : null}
        </View>
      </ScrollView>
    </View>
  );
};

export default FAQScreen;
