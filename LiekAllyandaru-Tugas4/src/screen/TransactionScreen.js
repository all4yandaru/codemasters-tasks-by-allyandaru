import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {textStyle} from '../assets/style/TextStyle';

const TransactionScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('TransactionRouting', {
              screen: 'DetailTransactionScreen',
            })
          }
          style={[localStyle.cardView]}>
          <Text style={[textStyle.size12BlackNormal, {color: '#BDBDBD'}]}>
            20 Desember 2020 09:00
          </Text>
          <Text style={[textStyle.size12BlackBold, {marginTop: 13}]}>
            New Balance - Pink Abu - 40
          </Text>
          <Text style={[textStyle.size12BlackNormal]}>Cuci Sepatu</Text>
          <View style={localStyle.rowContentBetween}>
            <Text style={[textStyle.size12BlackNormal]}>
              Kode Reservasi :{' '}
              <Text style={[textStyle.size12BlackBold]}>CS201201</Text>
            </Text>
            <Text style={localStyle.transactionStatus}>Reserved</Text>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  cardView: {
    backgroundColor: 'white',
    margin: 12,
    borderRadius: 8,
    padding: 16,
    elevation: 4,
  },
  rowContentBetween: {
    flexDirection: 'row',
    marginTop: 13,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  transactionStatus: {
    backgroundColor: '#F29C1F29',
    color: '#FFC107',
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 24,
  },
});
export default TransactionScreen;
