import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component, useState} from 'react';
import {styles} from '../assets/style/HomeStyle';
import CheckBox from '@react-native-community/checkbox';
import AntDesign from 'react-native-vector-icons/AntDesign';

const FormulisPemesanan = ({navigation}) => {
  const [data, setData] = useState([
    {key: 'Ganti Sol Sepatu', checked: false, id: 0},
    {key: 'Jahit Sepatu', checked: false, id: 1},
    {key: 'Repaint Sepatu', checked: false, id: 2},
    {key: 'Cuci Sepatu', checked: false, id: 3},
  ]);

  const handleItemPress = item => {
    const updatedData = data.map(x =>
      x.key === item.key ? {...x, checked: !x.checked} : x,
    );
    setData(updatedData);
  };

  return (
    <View style={[styles.safeAreaContent, {backgroundColor: 'white'}]}>
      <ScrollView
        style={{flexGrow: 1, marginHorizontal: 20, marginVertical: 24}}>
        <Text style={[localStyle.labelFormText, {marginTop: 8}]}>Merek</Text>
        <TextInput
          style={localStyle.textInputStyle}
          placeholderTextColor={'#C0C8E7'}
          placeholder="Masukkan Merk Barang"
        />
        <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Warna</Text>
        <TextInput
          style={localStyle.textInputStyle}
          placeholderTextColor={'#C0C8E7'}
          placeholder="Warna Barang, cth : Merah - Putih "
        />
        <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Ukuran</Text>
        <TextInput
          style={localStyle.textInputStyle}
          placeholderTextColor={'#C0C8E7'}
          placeholder="Cth : S, M, L / 39,40"
          keyboardType="numeric"
        />
        <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Photo</Text>
        <TouchableOpacity style={localStyle.photoInput}>
          <Image
            style={{width: 20, height: 18, marginBottom: 12}}
            source={require('../assets/camera.png')}
          />
          <Text>Button</Text>
        </TouchableOpacity>

        <FlatList
          style={{marginTop: 32}}
          // data={dataCheckBox}
          data={data}
          renderItem={({item}) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 4,
              }}>
              {/* <View
                style={{
                  height: 24,
                  width: 24,
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  marginRight: 16,
                }}>
                <AntDesign name="check" size={24} style={{color: 'green'}} />
              </View> */}
              <CheckBox
                value={item.checked}
                onValueChange={() => handleItemPress(item)}
              />
              <Text style={styles.item}>
                {item.key}, {item.checked ? 'benar' : 'salah'}, {item.id} abcd
              </Text>
            </View>
          )}
        />

        <Text style={[localStyle.labelFormText, {marginTop: 24}]}>Catatan</Text>
        <TextInput
          numberOfLines={4}
          multiline
          maxLength={40}
          style={[localStyle.textInputStyle, {textAlignVertical: 'top'}]}
          placeholderTextColor={'#C0C8E7'}
          placeholder="Warna Barang, cth : Merah - Putih "
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('KeranjangScreen')}
          style={localStyle.buttonStyle}>
          <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
            Masukkan Keranjang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const localStyle = StyleSheet.create({
  textInputStyle: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7,
    paddingHorizontal: 11,
    paddingVertical: 14,
    marginTop: 11,
  },
  labelFormText: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  photoInput: {
    alignSelf: 'baseline',
    alignItems: 'center',
    borderColor: '#BB2427',
    width: 84,
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 24,
    paddingTop: 24,
    paddingBottom: 12,
    paddingHorizontal: 8,
  },
  buttonStyle: {
    backgroundColor: '#BB2427',
    marginTop: 35,
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
});

export default FormulisPemesanan;
