import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../assets/style/HomeStyle';
import {textStyle} from '../assets/style/TextStyle';

const KeranjangScreen = ({navigation}) => {
  return (
    <View style={styles.safeAreaContent}>
      {/* MAIN CONTENT ====================================================================== */}
      <ScrollView style={{flexGrow: 1}}>
        {/* Card view */}
        <View style={localStyle.cardView}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('../assets/shoes_image.png')}
              style={localStyle.cardImage}
            />
            <View style={{marginHorizontal: 13, justifyContent: 'center'}}>
              <Text style={textStyle.size12BlackBold}>
                New Balance - Pink Abu - 40
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                Cuci Sepatu
              </Text>
              <Text style={[textStyle.size12BlackNormal, {color: '#737373'}]}>
                Note : -
              </Text>
            </View>
          </View>
        </View>
        {/* Tambah keranjang */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 43,
          }}>
          <Image
            style={localStyle.iconStyle}
            source={require('../assets/plus.png')}
          />
          <Text
            style={[
              textStyle.size14BlackBold,
              {color: '#BB2427', marginLeft: 8},
            ]}>
            Tambah Keranjang
          </Text>
        </View>
      </ScrollView>
      {/* FOOTER ===================================================================================== */}
      <View style={localStyle.footerContent}>
        <TouchableOpacity
          onPress={() => navigation.navigate('SummaryScreen')}
          style={localStyle.buttonStyle}>
          <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>
            Selanjutnya
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const localStyle = StyleSheet.create({
  cardView: {
    backgroundColor: 'white',
    paddingHorizontal: 14,
    paddingVertical: 24,
    borderRadius: 8,
    elevation: 4,
    margin: 11,
  },
  cardImage: {
    width: 84,
    height: 84,
    resizeMode: 'contain',
    borderRadius: 8,
  },
  buttonStyle: {
    backgroundColor: '#BB2427',
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  footerContent: {
    bottom: 0,
    marginBottom: 48,
    paddingHorizontal: 20,
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
  },
});
export default KeranjangScreen;
