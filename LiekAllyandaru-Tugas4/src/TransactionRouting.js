import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TransactionScreen from './screen/TransactionScreen';
import DetailScreen from './screen/DetailScreen';
import DetailTransactionScreen from './screen/DetailTransactionScreen';
import CheckOutScreen from './screen/CheckOutScreen';

const Stack = createStackNavigator();

const TransactionRouting = ({navigation}) => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="TransactionScreen">
      <Stack.Screen
        name="TransactionScreen"
        component={TransactionScreen}
        options={{headerShown: true, title: 'Transaksi'}}
      />
      <Stack.Screen
        name="DetailTransactionScreen"
        component={DetailTransactionScreen}
        options={{headerShown: true, title: ' '}}
      />
      <Stack.Screen
        name="CheckOutScreen"
        component={CheckOutScreen}
        options={{headerShown: true, title: 'Checkout'}}
      />
    </Stack.Navigator>
  );
};

export default TransactionRouting;
