import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  safeAreaContent: {
    backgroundColor: 'white',
    flex: 1,
  },
  scrollViewContent: {
    flexGrow: 1,
  },
  formContent: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 38,
    paddingBottom: 70,
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    marginTop: -38,
  },
  splashContent: {
    flex: 1,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashImage: {
    width: '100%',
    alignSelf: 'center',
  },
  footerContent: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    width: '100%',
    alignItems: 'center',
    paddingBottom: 22,
    paddingTop: 16,
    shadowColor: '#171717',
    shadowOpacity: 1,
    shadowRadius: 3,
    elevation: 12,
  },
  loginMethodContent: {
    marginTop: 18,
    flexDirection: 'row',
  },
  titleText: {
    fontSize: 24,
    fontWeight: '700',
    color: 'black',
  },
  labelFormText: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  textInputStyle: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7,
    paddingHorizontal: 11,
    paddingVertical: 14,
    marginTop: 11,
  },
  buttonStyle: {
    backgroundColor: '#BB2427',
    marginTop: 76,
    alignItems: 'center',
    padding: 19,
    borderRadius: 8,
  },
});

export {styles};
