import {StyleSheet} from 'react-native';

const textStyle = StyleSheet.create({
  size12BlackBold: {
    fontSize: 12,
    color: 'black',
    fontWeight: 'bold',
  },
  size14BlackBold: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  size16BlackBold: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
  },
  size18BlackBold: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
  },
  size36BlackBold: {
    fontSize: 36,
    color: 'black',
    fontWeight: 'bold',
  },
  size12BlackNormal: {
    fontSize: 12,
    fontWeight: 'normal',
    color: 'black',
  },
  size14BlackNormal: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'normal',
  },
  size16BlackNormal: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'black',
  },
  size18BlackNormal: {
    fontSize: 18,
    fontWeight: 'normal',
    color: 'black',
  },
});

export {textStyle};
