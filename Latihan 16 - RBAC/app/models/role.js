"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  const permissionRole = sequelize.define(
    "permissionRole",
    {},
    { timestamps: false, tableName: "permissionRole" }
  );
  class role extends Model {
    static associate(models) {
      models.role.hasMany(models.user);
      models.role.belongsToMany(models.permission, { through: permissionRole });
    }
  }
  role.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "role",
    }
  );
  return role;
};
