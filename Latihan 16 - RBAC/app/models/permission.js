"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  const permissionRole = sequelize.define(
    "permissionRole",
    {},
    { timestamps: false, tableName: "permissionRole" }
  );
  class permission extends Model {
    static associate(models) {
      models.permission.belongsToMany(models.role, { through: permissionRole });
    }
  }
  permission.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "permission",
    }
  );
  return permission;
};
