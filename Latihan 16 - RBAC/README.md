# EXPRESS JS with Role Based Access Control

materi:

- Express Js
- Middleware
- Nodemon untuk auto restart server
- connect ke mysql2
- express validator
- faker utk auto generate data palsu
- sequelize ORM
- sequelize-cli utk migrate data

cmd:
db migration:

- npx sequelize init
- npx sequelize model:generate --name role --attributes name:string
- npx sequelize model:generate --name permission --attributes name:string
- npx sequelize model:generate --name user --attributes name:string,email:string,roleId:integer
- npx sequelize model:generate --name token --attributes token:string,userId:integer
- npx sequelize migration:create --name create-permission-role

db seeder:

- npx sequelize seed:generate --name role-seeder
- npx sequelize seed:generate --name permission-seeder
- npx sequelize seed:generate --name user-seeder

- npx sequelize db:migrate:undo:all; npx sequelize db:migrate;
- npx sequelize db:seed:undo:all; npx sequelize db:seed:all

reference: https://github.com/harrymahardhika/express-rbac
