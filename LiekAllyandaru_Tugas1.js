/*
Soal Jaavscript

1. Javascrip berfungsi sebagai bahasa yang mengatur logika dan fungsi-fungsi yang dibuat. 
    javascript digunakan untuk berbagai kebutuhan web, mobile, dan backend seperti pada react native.
    javascript membantu produk agar lebih dinamis dan interaktif

2. a. var adalah variabel global yang dapat diubah nilainya
2. b. let adalah variabel lokal yang dapat diubah nilainya
2. c. const adalah variabel yang tidak dapat diubah nilainya setelah dideklarasikan

3. tipe data adalah jenis data yang dimiliki sebuah variabel,
    contoh:
    var num = 12 adalah number
    var car = "toyota" adalah string
    var isLogin = true adalah boolean

4. variabel adalah objek yang mengandung sebuah nilai number, string, boolean, null, ataupun undefined

5. string digabungkan dengan menggunakan simbol tambah +
    contoh : "aku " + "makan"

6. fungsi adalah instruksi yang sudah diurutkan dengan tujuan tertentu (misal fungsi perkalian berarti instruksi untuk melakukan perkalian)
    contoh:
    function perkalian(num1, num2){
        return num1*num2
    }

7. if digunakan untuk melakukan conditional statement dengan memberikan syarat/kondisi pada baris kode program
    contoh:
    if(hujan){
        console.log("tidak jadi pergi")
    } else {
        console.log("jadi pergi")
    }

8. array adalah variabel yang memuat banyak nilai dapat berupa number, string, boolean, ataupun objek lainnya
    contoh:
    var nilai = [1, 5, 8, 20, 9]
    console.log(nilai[1]) // output = 5

9. perulangan ada for loop dan while loop
    contoh:
    for (var index; condition; index++) {
        logic
    }

    while (condition) {
        logic
    }

10. callback adalah function yang dikirimkan ke fungsi lain untuk digunakan dalam kondisi tertentu
    contoh:
    function sapaDunia(nama, callback){
        console.log("hai, " + nama)
        callback()
    }

    function callbackPesan(){
        cosole.log("hari ini cerah")
    }

    sapaDunia("ujang", callbackPesan)
*/

// 1
var nama1 = "Liek Allyandaru, 23 tahun, Lampung"
let nama2 = "Liek Allyandaru, 23 tahun, Lampung"
const nama3 = "Liek Allyandaru, 23 tahun, Lampung"

console.log("no 1")
console.log(nama1)
console.log(nama2)
console.log(nama3)
console.log("")

// 2
var tanggal = 25
var bulan = 7

console.log("no 2")
console.log(tanggal+bulan)
console.log("")

// 3
function luasSegitiga(alas, tinggi){
    return 0.5*alas*tinggi
}

console.log("no 3")
console.log(luasSegitiga(6,8))
console.log("")

// 4
var buah = ["apel", "jeruk", "mangga"]

console.log("no 4")
console.log(buah[0])
console.log("")

// 5
var angka = 7

console.log("no 5")
if(angka > 5){
    console.log("Angka lebih besar dari 5")
} else {
    console.log("Angka kurang dari 5")
}
console.log("")

// 6
console.log("no 6")
for (let index = 1; index < 6; index++) {
    console.log(index)
    
}
console.log("")

// 7
var namaLengkap = {
    nama : "Liek Allyandaru",
    usia : 23
}
console.log("no 7")
console.log(namaLengkap.nama + " " + namaLengkap.usia + " tahun")
console.log("")

// 8
var hello = "hello"
var world = "world!"

console.log("no 8")
console.log(`${hello} ${world}`)
console.log("")

// 9
var nilai = 9

console.log("no 9")
if (nilai % 2 == 0) {
    console.log("genap")
} else {
    console.log("ganjil")
}
console.log("")

// 10
var nilaiArray = [3, 8, 2, 5, 1]

console.log("no 10")
for (let i = 0; i < nilaiArray.length-1; i++) {
    for (let j = i+1; j < nilaiArray.length; j++) {
        if (nilaiArray[i] > nilaiArray[j]) {
            let dum = nilaiArray[i]
            nilaiArray[i] = nilaiArray[j]
            nilaiArray[j] = dum
        }
    }
}

for (let i = 0; i < nilaiArray.length; i++) {
    console.log(`${nilaiArray[i]} `);
}