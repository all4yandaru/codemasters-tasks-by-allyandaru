import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Home from './Home';
import AddData from './AddData';

const Stack = createStackNavigator();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false, title: 'Home'}}
        />
        <Stack.Screen
          name="AddData"
          component={AddData}
          options={{headerShown: false, title: 'Add Data'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
