const express = require("express");
const Book = require("../models/Book");
const BookValidator = require("../support/validator/BookValidator");
const router = express.Router();

router.get("/", (req, res) => {
  Book.findAll().then((books) => {
    res.json(books);
  });
});

router.get("/:id", (req, res) => {
  Book.findByPk(req.params.id).then((book) => {
    res.json(book);
  });
});

router.post("/", new BookValidator().validate(), async (req, res) => {
  const newData = await Book.create(req.body).then(
    res.json({ status: 200, message: "insert success" })
  );
});

module.exports = router;
