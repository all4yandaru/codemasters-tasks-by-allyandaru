const { DataTypes } = require("sequelize");
const sequelize = require("../config/database");

const Author = sequelize.define(
  "author",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    underscored: true,
  }
);

module.exports = Author;
