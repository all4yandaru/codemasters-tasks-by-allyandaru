import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const SingupUI = () => {
  return (
    // KERANGKA ------------------------------------------------------------------------------------------------
    <SafeAreaView style={styles.safeParentContent}>
      {/* MAIN CONTENT----------------------------------------------------------------------------------------- */}
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        {/* LOGO */}
        <View style={styles.logoContent}>
          <Image style={styles.logo} source={require('./marlinLogo.png')} />
          <Text style={styles.logoText}>Create an account</Text>
        </View>

        {/* FORM */}
        <View style={styles.mainContent}>
          <TextInput
            style={styles.input}
            placeholder="Name"
            keyboardType="words"
            placeholderTextColor={'#959595'}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            keyboardType="words"
            placeholderTextColor={'#959595'}
          />
          <TextInput
            style={styles.input}
            placeholder="Phone"
            keyboardType="phone-pad"
            placeholderTextColor={'#959595'}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            keyboardType="words"
            secureTextEntry={true}
            placeholderTextColor={'#959595'}
          />
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      {/* FOOTER --------------------------------------------------------------------------------------------- */}
      <View style={styles.footerContent}>
        <Text style={styles.footerText}>
          Already have account?{' '}
          <Text style={{fontWeight: 'bold'}}>Sign in</Text>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeParentContent: {
    flex: 1,
  },
  scrollViewContent: {
    justifyContent: 'center',
    paddingHorizontal: 44,
    flexGrow: 1,
    paddingVertical: 48,
  },
  logoContent: {
    alignItems: 'center',
  },
  logo: {
    width: 200,
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 16,
  },
  mainContent: {
    marginTop: 33,
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    marginBottom: 16,
    borderRadius: 8,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 12,
  },
  footerContent: {
    backgroundColor: 'white',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    padding: 10,
    bottom: 0,
  },
  footerText: {
    fontSize: 11,
  },
});

export default SingupUI;
