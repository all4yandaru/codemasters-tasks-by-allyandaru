import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const SigninUI = () => {
  return (
    // KERANGKA ------------------------------------------------------------------------------------------------
    <SafeAreaView style={styles.safeParentContent}>
      {/* MAIN CONTENT----------------------------------------------------------------------------------------- */}
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        {/* LOGO */}
        <View style={styles.logoContent}>
          <Image style={styles.logo} source={require('./marlinLogo.png')} />
          <Text style={styles.logoText}>Please sign in to continue</Text>
        </View>

        {/* FORM */}
        <View style={styles.mainContent}>
          <TextInput
            style={styles.input}
            placeholder="Username"
            keyboardType="words"
            placeholderTextColor={'#959595'}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            keyboardType="words"
            secureTextEntry={true}
            placeholderTextColor={'#959595'}
          />
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Sign In</Text>
          </TouchableOpacity>
          <Text style={styles.smallText}>Forgot password</Text>

          {/* Line */}
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={styles.line} />
            <Text
              style={{
                textAlign: 'center',
                paddingHorizontal: 16,
                fontSize: 11,
                color: '#2E3283',
              }}>
              Login with
            </Text>
            <View style={styles.line} />
          </View>

          {/* Login Method */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 32,
            }}>
            <Image source={require('./facebook.png')} />
            <Image source={require('./google.png')} />
          </View>
          <Text style={{marginTop: 32, alignSelf: 'center'}}>
            App Version {'             '}2.8
          </Text>
        </View>
      </ScrollView>

      {/* FOOTER --------------------------------------------------------------------------------------------- */}
      <View style={styles.footerContent}>
        <Text style={styles.footerText}>
          Don't have account? <Text style={{fontWeight: 'bold'}}>Sign up</Text>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeParentContent: {
    flex: 1,
  },
  scrollViewContent: {
    justifyContent: 'center',
    paddingHorizontal: 44,
    flexGrow: 1,
    paddingVertical: 48,
  },
  logoContent: {
    alignItems: 'center',
  },
  logo: {
    width: 200,
  },
  logoText: {
    fontWeight: 'normal',
    fontSize: 18,
    marginTop: 16,
  },
  mainContent: {
    marginTop: 33,
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    marginBottom: 16,
    borderRadius: 8,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 12,
  },
  smallText: {
    fontSize: 11,
    marginTop: 16,
    marginBottom: 32,
    alignSelf: 'center',
    color: '#2E3283',
  },
  footerContent: {
    backgroundColor: 'white',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    padding: 10,
    bottom: 0,
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#959595',
  },
  footerText: {
    fontSize: 11,
  },
});

export default SigninUI;
