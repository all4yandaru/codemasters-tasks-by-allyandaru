import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';
import axios from 'axios';
import {deleteDataHelper, editDataHelper, postDataHelper} from './helpers';
import newHelpers from './newHelpers';

const AddData = ({modalVisible, onCloseModal, dataModal, setDataModal}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  useEffect(() => {
    if (dataModal) {
      const data = dataModal;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    } else {
      resetData();
    }
  }, [modalVisible]);

  const resetData = () => {
    setNamaMobil('');
    setTotalKM('');
    setHargaMobil('');
  };

  const exitModal = () => {
    resetData();
    onCloseModal();
  };

  const postData = () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    postDataHelper(body);
    exitModal();
  };

  const postDataNew = async () => {
    const token = '123487';
    const params = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const res = await newHelpers.postDataHome(params, token);
    console.log('response post mobil', res);
    if (res?.status === 201) {
      alert('success');
      // do something : loading false
    }
  };

  const editData = () => {
    const body = [
      {
        _uuid: dataModal._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    editDataHelper(body);
    exitModal();
  };

  const deleteData = () => {
    const body = [
      {
        _uuid: dataModal._uuid,
      },
    ];
    deleteDataHelper(body);
    exitModal();
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onCloseModal();
        setDataModal(null);
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            onCloseModal(false);
            setDataModal(null);
          }}
          style={{
            height: '100%',
            width: '100%',
            position: 'absolute',
            backgroundColor: 'black',
            opacity: 0.2,
          }}></TouchableOpacity>
        {/* ADD DATA */}
        <View
          style={{
            backgroundColor: 'white',
            margin: 16,
            padding: 8,
            paddingTop: 16,
            borderRadius: 16,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                color: '#000',
                marginLeft: 10,
              }}>
              {dataModal ? 'Update Data' : 'Tambah Data'}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              padding: 15,
            }}>
            <View>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Nama Mobil
              </Text>
              <TextInput
                onChangeText={text => setNamaMobil(text)}
                value={namaMobil}
                placeholder="Masukkan Nama Mobil"
                style={styles.txtInput}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Total Kilometer
              </Text>
              <TextInput
                onChangeText={text => setTotalKM(text)}
                value={totalKM}
                placeholder="contoh: 100 KM"
                style={styles.txtInput}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                Harga Mobil
              </Text>
              <TextInput
                onChangeText={text => setHargaMobil(text)}
                value={hargaMobil}
                placeholder="Masukkan Harga Mobil"
                style={styles.txtInput}
                keyboardType="number-pad"
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                dataModal ? editData() : postDataNew();
              }}
              style={styles.btnAdd}>
              <Text style={{color: '#fff', fontWeight: '600'}}>
                {dataModal ? 'Update Data' : 'Tambah Data'}
              </Text>
            </TouchableOpacity>
            {dataModal && (
              <TouchableOpacity
                onPress={() => deleteData()}
                style={[styles.btnAdd, {backgroundColor: 'red'}]}>
                <Text style={{color: '#fff', fontWeight: '600'}}>
                  Hapus Data
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
