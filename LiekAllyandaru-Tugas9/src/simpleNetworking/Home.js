import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  Modal,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component, useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import AddData from './AddData';
import axios from 'axios';
import {getDataMobilHelper} from './helpers';
import newHelpers from './newHelpers';
// import Carousel from 'react-native-snap-carousel';

const Home = ({navigation, route}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const isFocused = useIsFocused();

  const [dataMobil, setDataMobil] = useState([]);
  const [datamodal, setDatamodal] = useState(null);

  useEffect(() => {
    getDataMobilHelper(setDataMobil, setIsLoading);
    getData();
    setIsLoading(true);
    console.log('helpers: ', newHelpers);
    console.log('is focused: ', isFocused);
  }, [isFocused]);

  const getData = async () => {
    const res = await newHelpers.getDataHome();
    console.log('response get Mobil', res);
    if (res?.items?.length) {
      // do something set data
    }
  };

  const showLoading = () => {
    return (
      <Modal animationType="fade" transparent={true} visible={isLoading}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 16,
              backgroundColor: 'white',
              borderRadius: 32,
              elevation: 2,
            }}>
            <Image
              source={require('../assets/lottielego.gif')}
              style={{width: 100, height: 100}}
            />
          </View>
        </View>
      </Modal>
    );
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      {/* MODAL POP UP ============================================================ */}
      {showLoading()}
      {/* {showAdd()} */}
      <AddData
        modalVisible={modalVisible}
        onCloseModal={() => {
          setModalVisible(false);
          getDataMobilHelper(setDataMobil, setIsLoading);
        }}
        dataModal={datamodal}
        setDataModal={setDatamodal}
      />

      {/* LIST ITEM =================================================================================== */}
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      {/* <Carousel
        layout={'default'}
        data={dataMobil}
        sliderWidth={300}
        itemWidth={300}
        renderItem={(item, index) => {
          <View
            style={{
              backgroundColor: 'floralwhite',
              borderRadius: 5,
              height: 250,
              padding: 50,
              marginLeft: 25,
              marginRight: 25,
            }}>
            <Text style={{fontSize: 30}}>{item.title}</Text>
          </View>;
        }}
      /> */}
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => {
              // navigation.navigate('AddData', item)
              setModalVisible(true);
              setDatamodal(item);
            }}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {'Rp '}
                  {convertCurrency(item.harga)}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />

      {/* PLUS BUTTON ======================================================================== */}
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => {
          setModalVisible(true);
          setDatamodal(null);
        }}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  safeAreaContent: {
    flex: 1,
    backgroundColor: 'red',
  },
});

export default Home;
