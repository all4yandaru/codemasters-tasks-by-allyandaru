/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import SigninUI from './SigninUI';
import SingupUI from './SignupUI';
import ForgotPasswordUI from './ForgotPassword';
import MyStack from './RouteUI';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MyStack);
