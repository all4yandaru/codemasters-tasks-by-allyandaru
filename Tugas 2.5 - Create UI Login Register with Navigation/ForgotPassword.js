import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const ForgotPasswordUI = ({navigation}) => {
  return (
    // KERANGKA ------------------------------------------------------------------------------------------------
    <SafeAreaView style={styles.safeParentContent}>
      {/* HEADER ---------------------------------------------------------------------------------------------- */}
      <View style={styles.headerContent}>
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}>
          <Image source={require('./back_button.png')} />
        </TouchableOpacity>
      </View>
      {/* MAIN CONTENT----------------------------------------------------------------------------------------- */}
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        {/* LOGO */}
        <View style={styles.logoContent}>
          <Image style={styles.logo} source={require('./marlinLogo.png')} />
          <Text style={styles.logoText}>Reset your password</Text>
        </View>

        {/* FORM */}
        <View style={styles.mainContent}>
          <TextInput
            style={styles.input}
            placeholder="Email"
            keyboardType="words"
            placeholderTextColor={'#959595'}
          />
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Request Reset</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeParentContent: {
    flex: 1,
  },
  scrollViewContent: {
    justifyContent: 'center',
    paddingHorizontal: 44,
    flexGrow: 1,
    paddingVertical: 48,
  },
  logoContent: {
    alignItems: 'center',
  },
  logo: {
    width: 200,
  },
  logoText: {
    fontWeight: 'normal',
    fontSize: 18,
    marginTop: 40,
  },
  mainContent: {
    marginTop: 16,
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    marginBottom: 16,
    borderRadius: 8,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
  },
  backButton: {
    marginTop: 40,
    marginLeft: 20,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 12,
  },
  headerContent: {
    alignSelf: 'baseline',
    position: 'absolute',
    zIndex: 1,
  },
  footerText: {
    fontSize: 11,
  },
});

export default ForgotPasswordUI;
