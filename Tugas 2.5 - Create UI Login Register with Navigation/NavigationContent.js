import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import HomeScreen from './navigation/HomeScreen';
import ProfileScreen from './navigation/ProfileScreen';
import MyTabBar from './MyTabBar';

const Tab = createBottomTabNavigator();

const NavigationContent = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Booking" component={ProfileScreen} />
      <Tab.Screen name="Help" component={ProfileScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
};

export default NavigationContent;

// Cara Alternatif ==============================================================================
{
  /* <Tab.Screen
  name="Home"
  component={HomeScreen}
  options={{
    title: 'Home Screen',
    tabBarIcon: ({focused}) => (
      <Image
        source={focused ? require('./Home.png') : require('./Home.png')}
      />
    ),
  }}
/> */
}

{
  /* 
<Image
source={
  label == 'Home'
  ? require('./Home.png')
  : label == 'Profile'
  ? require('./Profile.png')
  : require('./Help.png')
}
/> */
}
