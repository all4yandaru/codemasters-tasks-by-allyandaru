import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              backgroundColor: 'white',
              alignItems: 'center',
              paddingVertical: 5,
            }}>
            <Image source={checkLogo(label)} />
            <Text style={{color: isFocused ? '#2E3283' : '#222'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
const checkLogo = label => {
  if (label == 'Home') {
    return require('./images/Home.png');
  } else if (label == 'Profile') {
    return require('./images/Profile.png');
  } else if (label == 'Help') {
    return require('./images/Help.png');
  } else if (label == 'Booking') {
    return require('./images/Booking.png');
  }
};

export default MyTabBar;
