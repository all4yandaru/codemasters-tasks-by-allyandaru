import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SigninUI from './SigninUI';
import SingupUI from './SignupUI';
import ForgotPasswordUI from './ForgotPassword';
import NavigationContent from './NavigationContent';

const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Signin">
        <Stack.Screen name="Signin" component={SigninUI} />
        <Stack.Screen name="Signup" component={SingupUI} />
        <Stack.Screen name="Forgot" component={ForgotPasswordUI} />
        <Stack.Screen name="NavContent" component={NavigationContent} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MyStack;
