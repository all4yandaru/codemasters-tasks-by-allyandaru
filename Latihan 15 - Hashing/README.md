# EXPRESS JS with Authentication

materi:

- Express Js
- Middleware
- Nodemon untuk auto restart server
- connect ke mysql2
- express validator
- faker utk auto generate data palsu
- sequelize ORM
- sequelize-cli utk migrate data

cmd:
db migration:

- npx sequelize init
- npx sequelize model:generate --name book --attributes title:string,authorId:integer
- npx sequelize db:migrate

db seeder:

- npx sequelize seed:generate --name book-seeder
- npx sequelize db:seed:all
