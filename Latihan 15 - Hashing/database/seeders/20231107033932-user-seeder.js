"use strict";

const { faker } = require("@faker-js/faker");
const { user } = require("../../app/models");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    for (let index = 0; index < 10; index++) {
      await user.create({
        name: faker.company.name(),
        email: faker.internet.email(),
        password: "pass123",
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("tokens", null, {});
    await queryInterface.bulkDelete("users", null, {});
  },
};
