const express = require("express");
const { user } = require("../models");
const router = express.Router();

router.get("/", (req, res) => {
  user.findAll().then((users) => {
    res.json(users);
  });
});

// router.get("/:id", (req, res) => {
//     user.findByPk(req.params.id, { include: "Author" }).then((book) => {
//     res.json(book);
//   });
// });

// router.post("/", new BookValidator().validate(), async (req, res) => {
//   const newData = await user.create(req.body).then(
//     res.json({ status: 200, message: "insert success" })
//   );
// });

module.exports = router;
