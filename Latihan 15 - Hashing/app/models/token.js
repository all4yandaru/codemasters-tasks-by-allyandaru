"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class token extends Model {
    static associate(models) {
      models.token.belongsTo(models.user);
    }
  }
  token.init(
    {
      userId: DataTypes.INTEGER,
      token: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "token",
    }
  );
  return token;
};
