require("dotenv").config();
const bcrypt = require("bcrypt");

const bcryptRound = process.env.BCRYPT_ROUND || 10;
const password = "myPassword";
const checkPassword = "mysPassword";

const hashHere = async (hashPass) => {
  const hashValue = await bcrypt.hash(hashPass, parseInt(bcryptRound));
  return hashValue;
};

const compareHere = async (plainPass, hashPass) => {
  const compareValue = await bcrypt.compare(plainPass, hashPass);
  return compareValue;
};

const main = async () => {
  const hashPassword = await hashHere(password);
  const comparePassword = await compareHere(checkPassword, hashPassword);

  console.log("hash password: ", hashPassword);
  console.log("check password: ", comparePassword);
};

main();
