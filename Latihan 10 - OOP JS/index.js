import { Bird } from "./Bird.js";
import Professor from "./Professor.js";

const daru = new Professor("Daru", "Math");

daru.teach();

const manuk_dadali = new Bird("Manuk Dadali", 2);

manuk_dadali.speak();
