import { Animal } from "./Animal.js";

class Bird extends Animal {
  constructor(name, age = 1) {
    super(name);
    this.age = age;
  }

  speak(message = "hi there") {
    console.log(`hi i am ${this.name}, i'm ${this.age} y.o, ${message}`);
  }
}

export { Bird };
