export default class Professor {
  constructor(name = "myName", job = "myJob") {
    this.name = name;
    this.job = job;
  }

  introduceSelf() {
    console.log(`hi, my name is ${this.name}`);
  }

  teach() {
    console.log(`${this.name} teaches ${this.job}`);
  }
}
