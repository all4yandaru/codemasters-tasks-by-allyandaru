require("dotenv").config();
const express = require("express");
const mysql = require("mysql2");
const app = express();

app.use(express.json());

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT || 3306,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

// MIDDLEWARE ===============================================================
const dateMiddleware = (req, res, next) => {
  console.log("Time: ", Date.now());
  next();
};

const loggerMiddleware = (req, res, next) => {
  console.log(`request to ${req.url} with method ${req.method}`);
  next();
};

app.use(dateMiddleware);
app.use(loggerMiddleware);

const throwError = (res, message, status = 500) => {
  res.status(status).json({ status: status, message: message });
};

const throwSuccess = (res, message, status = 200) => {
  res.status(status).json({ status: status, message: message });
};

const checkBody = (body) => {
  return Object.keys(body).length > 0 ? true : false;
};

// GET ======================================================================
// GET All Books
app.get("/api/bookstore/books", (req, res) => {
  db.query("SELECT * FROM books", (error, result, fields) => {
    if (error) {
      throwError(res, "cannot get books data", 500);
    } else {
      res.status(201).json(result);
    }
  });
});
// GET a Book
app.get("/api/bookstore/books/:id", (req, res) => {
  const bookId = req.params.id;
  db.query(
    `SELECT * FROM books WHERE id=${bookId}`,
    (error, result, fields) => {
      if (error) {
        throwError(res, `cannot get book with id ${bookId}`, 500);
      } else if (result.length < 1) {
        throwError(res, `cannot get book with id ${bookId}`, 500);
      } else {
        res.status(201).json(result);
      }
    }
  );
});

// POST ====================================================================
// POST a Book
app.post("/api/bookstore/books", (req, res) => {
  const newBookData = req.body;

  if (checkBody(newBookData)) {
    db.query(
      "INSERT INTO books SET ?",
      newBookData,
      (error, result, fields) => {
        if (error) {
          throwError(res, error, 500);
        } else {
          throwSuccess(res, "book successfully added to database", 200);
        }
      }
    );
  } else {
    throwError(res, "no body", 400);
  }
});

// DELETE ==================================================================
// DELETE a Book

// LISTEN ==================================================================
app.listen(process.env.APP_PORT, () => {
  console.log(`server is running on ${process.env.APP_PORT}`);
});
