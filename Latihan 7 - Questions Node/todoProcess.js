const tasks = [];

function addTask(task) {
  tasks.push({ task, completed: false });
}

function listTasks() {
  tasks.forEach((task, index) => {
    console.log(`${index + 1}. ${task.task} [${task.completed ? "✅️" : " "}]`);
  });
}

function completeTask(taskIndex) {
  if (taskIndex >= 0 && taskIndex < tasks.length) {
    tasks[taskIndex].completed = true;
  }
}

function deleteTask(taskIndex) {
  if (taskIndex >= 0 && taskIndex < tasks.length) {
    tasks.splice(taskIndex, 1);
  }
}

module.exports = { addTask, listTasks, completeTask, deleteTask };
