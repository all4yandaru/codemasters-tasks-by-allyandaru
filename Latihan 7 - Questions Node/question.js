const fs = require("fs");
const questions = require("./questions.json");

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function shuffle(array) {
  let currentIndex = array.length,
    randomIndex;
  // While there remain elements to shuffle.
  while (currentIndex > 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }
  return array;
}

const quiz = questions.questions;
shuffle(quiz);

var quizIndex = 0;
var score = 0;

function askMe() {
  console.log("pertanyaan ke ", quizIndex);
  const ask = quiz[quizIndex];
  if (quizIndex < quiz.length) {
    rl.question(`${ask.question} `, (answer) => {
      if (answer == ask.answer) {
        score++;
      }
      quizIndex++;
      askMe();
    });
  } else {
    console.log("Your final score is ", score);
    rl.close();
  }
}

askMe();
