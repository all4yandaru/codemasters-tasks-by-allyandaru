const readline = require("readline");
const {
  addTask,
  listTasks,
  completeTask,
  deleteTask,
} = require("./todoProcess.js");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function main() {
  console.log("To-Do List Application");
  showMenu();
}

function showMenu() {
  console.log("\nOptions:");
  console.log("1. Add a task");
  console.log("2. List tasks");
  console.log("3. Complete a task");
  console.log("4. Delete a task");
  console.log("5. Quit");

  rl.question("Select an option: ", (choice) => {
    switch (choice) {
      case "1":
        rl.question("Enter the task: ", (task) => {
          addTask(task);
          console.log(`Task "${task}" added to the to-do list.`);
          showMenu();
        });
        break;
      case "2":
        console.log("\nTo-Do List:");
        listTasks();
        showMenu();
        break;
      case "3":
        rl.question("Enter the task number to complete: ", (index) => {
          completeTask(index - 1);
          console.log(`Task marked as completed.`);
          showMenu();
        });
        break;
      case "4":
        rl.question("Enter the task number to delete: ", (index) => {
          deleteTask(index - 1);
          console.log(`Task deleted from the to-do list.`);
          showMenu();
        });
        break;
      case "5":
        console.log("Goodbye!");
        rl.close();
        break;
      default:
        console.log("Invalid option. Please try again.");
        showMenu();
        break;
    }
  });
}

main();
