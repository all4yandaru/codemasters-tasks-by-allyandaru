require("dotenv").config();
const { v1: uuidv1, v4: uuidv4 } = require("uuid");
const express = require("express");
const mysql = require("mysql2");
const app = express();

app.use(express.json());

const products = [
  { id: 1, name: "Produk A" },
  { id: 2, name: "Produk B" },
  { id: 3, name: "Produk C" },
];

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT || 3306,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

const checkId = (id) => {
  return products.find((val) => val.id == id) ? true : false;
};

// MIDDLEWARE ===============================================================
// midware 1
app.use((req, res, next) => {
  console.log("Time: ", Date.now());
  next();
});

// midware 2
const loggerMiddleware = (req, res, next) => {
  console.log(`request to ${req.url} with method ${req.method}`);
  next();
};

app.use(loggerMiddleware);

// REST EXPRESS API INDO_REGION (WITH NODEMON) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
// get all provinces
app.get("/api/region/provinces", (req, res) => {
  db.query("SELECT * FROM provinces", (error, result, fields) => {
    if (error) throw error;
    res.status(201).json(result);
  });
});

// post data province
app.post("/api/region/provinces", (req, res) => {
  const newData = req.body;

  db.query("INSERT INTO provinces SET ?", newData, (error, result) => {
    if (error) {
      res.status(400).json({
        status: 400,
        description: "unprocessable data",
      });
    }
    console.log("result: ", result);
    res.status(201).json(newData);
  });
});

// REST EXPRESS API PRODUCTS (WITHOUT NODEMON) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
// GET DATA =================================================================
// get all data
app.get("/api/products", (req, res) => {
  res.status(201).json(products);
});

// get detail data
app.get("/api/products/:id", (req, res) => {
  const searchId = req.params.id;
  const searchData = products.find((val) => val.id == searchId);
  res.status(201).json(searchData);
});

app.get("/api/products/search/:name", (req, res) => {
  const searchName = req.params.name;
  const searchData = products.find((val) => val.name == searchName);
  res.status(201).json(searchData);
});

// POST DATA ================================================================
// add new data
app.post("/api/products", (req, res) => {
  const name = req.body.name;

  const body = {
    id: uuidv4(),
    name: name,
  };

  if (checkId(body.id)) {
    res.status(422).json({ status: 422, description: "Unprocessable Entity" });
  } else {
    products.push(body);
    res.status(201).json(products);
  }
});

// LISTEN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
app.listen(process.env.APP_PORT, () => {
  console.log(`server is running on ${process.env.APP_PORT}`);
});
