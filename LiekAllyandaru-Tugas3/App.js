import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {styles} from './style/styles';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import MainScreen from './navigation/MainScreen';
import NetInfoScreen from './navigation/NetInfoScreen';
import DateTimePickerScreen from './navigation/DateTimePicker';
import SliderScreen from './navigation/SliderScreen';
import GeoScreen from './navigation/GeoScreen';
import ProgressViewScreen from './navigation/ProgressViewScreen';
import ProgressBarAndroidScreen from './navigation/ProgressBarAndroidScreen';
import ClipboardScreen from './navigation/ClipboardScreen';
import AsyncScreen from './navigation/AsyncScreen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="MainScreen">
        <Stack.Screen name="MainScreen" component={MainScreen} />
        <Stack.Screen name="NetInfoScreen" component={NetInfoScreen} />
        <Stack.Screen
          name="DateTimePickerScreen"
          component={DateTimePickerScreen}
        />
        <Stack.Screen name="SliderScreen" component={SliderScreen} />
        <Stack.Screen name="GeoScreen" component={GeoScreen} />
        <Stack.Screen
          name="ProgressViewScreen"
          component={ProgressViewScreen}
        />
        <Stack.Screen
          name="ProgressBarAndroidScreen"
          component={ProgressBarAndroidScreen}
        />
        <Stack.Screen name="ClipboardScreen" component={ClipboardScreen} />
        <Stack.Screen name="AsyncScreen" component={AsyncScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
