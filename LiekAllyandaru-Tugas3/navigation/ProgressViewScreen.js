import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../style/styles';
import {ProgressView} from '@react-native-community/progress-view';
const ProgressViewScreen = () => {
  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text style={styles.TextTitleStyle}>ProgressViewScreen</Text>
        <ProgressView
          style={{width: '100%'}}
          progressTintColor="red"
          trackTintColor="blue"
          progress={0.7}
          progressViewStyle="default"
        />
        <Text style={styles.TextTitleStyle}>ProgressViewScreen</Text>
      </View>
    </View>
  );
};

export default ProgressViewScreen;
