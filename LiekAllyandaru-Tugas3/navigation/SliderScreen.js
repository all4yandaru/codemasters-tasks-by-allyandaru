import {Text, View} from 'react-native';
import React, {Component, useState} from 'react';
import {styles} from '../style/styles';
import Slider from '@react-native-community/slider';

const SliderScreen = () => {
  const [sliderValue, setSliderValue] = useState(0);

  const changeSliderValue = value => {
    setSliderValue(value);
  };

  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text>Slider Screen</Text>
        <Text>value: {sliderValue}</Text>
        <Slider
          style={{width: 200, height: 40}}
          minimumValue={0}
          maximumValue={100}
          step={1}
          minimumTrackTintColor="blue"
          thumbTintColor="black"
          maximumTrackTintColor="#000000"
          onValueChange={value => changeSliderValue(value)}
        />
      </View>
    </View>
  );
};

export default SliderScreen;
