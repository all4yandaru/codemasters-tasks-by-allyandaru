import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {styles} from '../style/styles';

const MainScreen = ({navigation}) => {
  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text style={styles.TextTitleStyle}>React Native Library</Text>
        {/* Netinfo ============================================================================ */}
        <TouchableOpacity
          onPress={() => navigation.navigate('NetInfoScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'white'}]}>
          <Text style={{color: 'black'}}>Netinfo</Text>
        </TouchableOpacity>

        {/* Date Time Picker =================================================================== */}
        <TouchableOpacity
          onPress={() => navigation.navigate('DateTimePickerScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'blue'}]}>
          <Text style={{color: 'white'}}>Date Time Picker</Text>
        </TouchableOpacity>

        {/* Slider ============================================================================= */}
        <TouchableOpacity
          onPress={() => navigation.navigate('SliderScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'green'}]}>
          <Text style={{color: 'white'}}>Slider</Text>
        </TouchableOpacity>

        {/* Geo Location ======================================================================= */}
        <TouchableOpacity
          onPress={() => navigation.navigate('GeoScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'gray'}]}>
          <Text style={{color: 'white'}}>Geo Location</Text>
        </TouchableOpacity>

        {/* Progress View ======================================================================= */}
        <TouchableOpacity
          onPress={() => navigation.navigate('ProgressViewScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'black'}]}>
          <Text style={{color: 'white'}}>Progress View</Text>
        </TouchableOpacity>

        {/* Progress Bar Android ============================================================= == */}
        <TouchableOpacity
          onPress={() => navigation.navigate('ProgressBarAndroidScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'red'}]}>
          <Text style={{color: 'white'}}>Progress Bar Android</Text>
        </TouchableOpacity>

        {/* Clipboard ============================================================= == */}
        <TouchableOpacity
          onPress={() => navigation.navigate('ClipboardScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'aqua'}]}>
          <Text style={{color: 'white'}}>ClipboardScreen</Text>
        </TouchableOpacity>

        {/* Async Storage ============================================================= == */}
        <TouchableOpacity
          onPress={() => navigation.navigate('AsyncScreen')}
          style={[styles.ButtonContentStyle, {backgroundColor: 'pink'}]}>
          <Text style={{color: 'white'}}>Async Storage</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MainScreen;
