import {Text, View} from 'react-native';
import React, {Component} from 'react';
import {styles} from '../style/styles';
import {ProgressBar} from '@react-native-community/progress-bar-android';

const ProgressBarAndroidScreen = () => {
  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text style={styles.TextTitleStyle}>ProgressBarAndroid</Text>
        <ProgressBar />
        <ProgressBar styleAttr="Horizontal" />
        <ProgressBar styleAttr="Horizontal" color="#2196F3" />
        <ProgressBar styleAttr="Small" color="#2196F3" />
        <ProgressBar styleAttr="Large" color="#2196F3" />
        <ProgressBar styleAttr="Inverse" color="#2196F3" />
        <ProgressBar styleAttr="SmallInverse" color="#2196F3" />
        <ProgressBar styleAttr="LargeInverse" color="#2196F3" />
        <ProgressBar
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        />
      </View>
    </View>
  );
};

export default ProgressBarAndroidScreen;
