import {Text, TouchableOpacity, View} from 'react-native';
import React, {Component, useState} from 'react';
import {styles} from '../style/styles';
import Clipboard from '@react-native-community/clipboard';
import {TextInput} from 'react-native-gesture-handler';

const ClipboardScreen = () => {
  const [copiedText, setCopiedText] = useState('');
  const [inputText, setInputText] = useState('');

  const copyToClipboard = () => {
    Clipboard.setString('hello world');
  };

  const fetchCopiedText = async () => {
    const text = await Clipboard.getString();
    setCopiedText(text);
  };

  const copyFromTextInput = text => {
    setInputText(text);
  };

  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <TouchableOpacity onPress={copyToClipboard}>
          <Text>Click here to copy to Hello World!</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={fetchCopiedText}>
          <Text>View copied text</Text>
        </TouchableOpacity>

        <Text style={styles.copiedText}>{copiedText}</Text>

        <TextInput
          style={{
            backgroundColor: 'white',
            width: '100%',
            borderRadius: 8,
            marginTop: 16,
          }}
          placeholder="Input text"
          onChangeText={newText => copyFromTextInput(newText)}
        />
        <TouchableOpacity
          onPress={() => {
            setCopiedText(inputText);
            Clipboard.setString(inputText);
          }}
          style={styles.ButtonContentStyle}>
          <Text>Copy</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ClipboardScreen;
