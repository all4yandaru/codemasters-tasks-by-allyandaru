import React, {Component, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {styles} from '../style/styles';

const NetInfoScreen = () => {
  const [netinfo, useNetInfo] = useState('');

  const netInformation = NetInfo.useNetInfo();

  const handleGetNetInfo = () => {
    NetInfo.fetch().then(state => {
      console.log(state);
      alert(
        'connection type:' +
          state.type +
          ' isConnected:' +
          state.isConnected +
          ' isInternetReachable:' +
          state.isInternetReachable +
          ' isWifiEnabled:' +
          state.isWifiEnabled +
          ' \ncarrier:' +
          state.details.carrier +
          ' \nconnection expensive:' +
          state.details.ipAddress,
      );
    });
  };

  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text>type: {netInformation.type}</Text>
        <Text>
          isConnected:{' '}
          {netInformation.isConnected
            ? netInformation.isConnected.toString()
            : 'null'}
        </Text>
        <Text>
          isInternetReachable:{' '}
          {netInformation.isInternetReachable
            ? netInformation.isInternetReachable.toString()
            : 'null'}
        </Text>
        <TouchableOpacity
          onPress={() => handleGetNetInfo()}
          style={[styles.ButtonContentStyle, {backgroundColor: 'white'}]}>
          <Text style={{color: 'black'}}>Netinfo</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NetInfoScreen;
