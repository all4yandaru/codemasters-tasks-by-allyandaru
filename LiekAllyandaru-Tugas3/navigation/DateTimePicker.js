import React, {Component, useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {styles} from '../style/styles';
import RNDateTimePicker, {
  DateTimePickerAndroid,
} from '@react-native-community/datetimepicker';

const DateTimePicker = () => {
  const [isPickerShow, setIsPickerShow] = useState(false);
  const [date, setDate] = useState(new Date(Date.now()));

  const updateTime = () => {
    setDate(new Date(Date.now()));
  };

  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        {/* Display the curent date ================================================================== */}
        <Text>{date.toUTCString()}</Text>
        <Button title="Update Time" onPress={updateTime} />
        <Button title="Show Calendar" onPress={() => setIsPickerShow(true)} />
        {/* Time Picker ============================================================================== */}
        {isPickerShow ? (
          <RNDateTimePicker
            onChange={(event, selected) => {
              setIsPickerShow(false);
              setDate(selected);
            }}
            // onTouchCancel={() => setIsPickerShow(false)}
            display="calendar"
            mode="date"
            value={new Date()}
          />
        ) : null}
      </View>
    </View>
  );
};

export default DateTimePicker;
