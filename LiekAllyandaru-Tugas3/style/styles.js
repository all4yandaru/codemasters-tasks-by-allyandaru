import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  SafeAreaStyle: {
    padding: 24,
    flex: 1,
  },
  MainContentStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  TextTitleStyle: {
    fontSize: 24,
    marginVertical: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  ButtonContentStyle: {
    backgroundColor: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export {styles};
